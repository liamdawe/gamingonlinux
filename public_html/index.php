<?php
define("APP_ROOT", dirname(__FILE__));
define('golapp', TRUE);

include(APP_ROOT . '/includes/header.php');

$core->url_paths();
if (core::$current_module['module_file_name'] == 'home')
{
	$total_featured = $core->config('total_featured');

	if ($total_featured >= 2)
	{
		$templating->block('featured_multiple', 'mainpage');

		$featured_list = $dbl->run("SELECT a.article_id, a.`title`, a.`active`, a.`thumbnail_alt_text`, a.`date`, a.`slug`, a.`tagline_image`, a.`gallery_tagline`, g.`filename` FROM `editor_picks` p INNER JOIN `articles` a ON a.article_id = p.article_id LEFT JOIN `articles_tagline_gallery` g ON g.`id` = a.`gallery_tagline` WHERE a.active = 1 AND p.`end_date` > now() ORDER BY RAND() LIMIT 2")->fetch_all(PDO::FETCH_BOTH);

		$counter = 0;
		foreach ($featured_list as $item)
		{
			$thumbnail_alt_text = '';
			if (!empty($item['thumbnail_alt_text']))
			{
				$thumbnail_alt_text = $item['thumbnail_alt_text'];
			}

			if ($item['gallery_tagline'] != 0)
			{
				$thumb_url = url . 'uploads/tagline_gallery/' . $item['filename'];
			}
			else
			{
				$thumb_url = url . 'uploads/articles/tagline_images/'.$item['tagline_image'];
			}

			$image = '<img height="300" width="634" src="'.$thumb_url.'" alt="'.$thumbnail_alt_text.'">';

			if ($counter == 0)
			{
				$templating->set('feature_image_big', $image);
				$templating->set('big-title', $item['title']);
			}
			else if ($counter == 1)
			{
				$templating->set('feature_image_littletop', $image);
				$templating->set('toplittle-title', $item['title']);
			}
			else if ($counter == 2)
			{
				$templating->set('feature_image_littlebottom', $image);
				$templating->set('bottomlittle-title', $item['title']);
			}

			$article_link = $article_class->article_link(array('date' => $item['date'], 'slug' => $item['slug']));

			$templating->set('featured-link-'.$counter, $article_link);

			$counter++;
		}
	}
}

/* announcement bars */
$announcements = $announcements_class->get_announcements();

if (!empty($announcements))
{
	$templating->load('announcements');
	$templating->block('announcement_top', 'announcements');
	$templating->block('announcement', 'announcements');
	$templating->set('text', $bbcode->parse_bbcode($announcements['text']));
	$templating->set('dismiss', $announcements['dismiss']);
	$templating->block('announcement_bottom', 'announcements');
}

// show user warnings
if (isset($_SESSION['user_id']) && $_SESSION['user_id'] > 0)
{
	$check_warnings = $dbl->run("SELECT 1 FROM `user_notifications` WHERE `type` = 'warning' AND `owner_id` = ?", array($_SESSION['user_id']))->fetchOne();
	if ($check_warnings)
	{
		$templating->load('announcements');
		$templating->block('announcement_top', 'announcements');
		$templating->block('warning', 'announcements');
		$templating->set('text', '<strong>NOTICE:</strong> You have an account-level warning. Check and dismiss this <a href="'.url.'/usercp.php?module=notifications">from your notifications</a>. Warnings can also be seen at the bottom on your profile page (not public).');
		$templating->block('announcement_bottom', 'announcements');
	}
}

// let them know they aren't activated yet
if (!isset($_SESSION['activated']) && $_SESSION['user_id'] != 0)
{
	$get_active = $dbl->run("SELECT `activated` FROM `users` WHERE `user_id` = ?", array($_SESSION['user_id']))->fetch();
	$_SESSION['activated'] = $get_active['activated'];
}

if (isset($_SESSION['activated']) && $_SESSION['activated'] == 0)
{
	if ( (isset($_SESSION['message']) && $_SESSION['message'] != 'new_account') || !isset($_SESSION['message']))
	{
		$templating->block('activation', 'mainpage');
		$templating->set('url', $core->config('website_url'));
	}
}

$templating->block('left', 'mainpage');

// so mainpage.html knows to put "articles" class in the left block or not
if (core::$current_module['module_file_name'] == 'home' || core::$current_module['module_file_name'] == 'search' || core::$current_module['module_file_name'] == 'hot_articles' || core::$current_module['module_file_name'] == 'hot_articles_week' || (core::$current_module['module_file_name'] == 'articles' && isset($_GET['view']) && ($_GET['view'] == 'cat' || $_GET['view'] == 'multiple')))
{
	$articles_css = 'articles';
}
else 
{
	$articles_css = '';
}
$templating->set('articles_css', $articles_css);

if (isset($_SESSION['message']))
{
	$extra = NULL;
	if (isset($_SESSION['message_extra']))
	{
		$extra = $_SESSION['message_extra'];
	}
	$message_map->display_message(core::$current_module['module_file_name'], $_SESSION['message'], $extra);
}

// top of website adverts above content
if (!isset($_SESSION['hide_adverts']) || (isset($_SESSION['hide_adverts']) && $_SESSION['hide_adverts'] == 0))
{
	if (!isset($_SERVER['REQUEST_URI']) || (isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], 'direct-messages') === false && strpos($_SERVER['REQUEST_URI'], 'submit-article') === false))
	{
		$templating->block('adverts', 'mainpage');
	}
}

include('modules/'.core::$current_module['module_file_name'].'.php');

$templating->block('left_end', 'mainpage');

// The block that starts off the html for the left blocks
$templating->block('right', 'mainpage');

/* get the blocks */
if (($blocks = unserialize($core->get_dbcache('index_blocks'))) === false) // there's no cache
{
	$blocks = $dbl->run('SELECT `block_link`, `block_id`, `block_title_link`, `block_title`, `block_custom_content`, `style`, `nonpremium_only`, `homepage_only` FROM `blocks` WHERE `activated` = 1 ORDER BY `order`')->fetch_all(PDO::FETCH_BOTH);
	$core->set_dbcache('index_blocks', serialize($blocks)); // no expiry as shown blocks hardly ever changes
}

foreach ($blocks as $block)
{
	// PHP BLOCKS
	if ($block['block_link'] != NULL)
	{
		include(APP_ROOT . "/blocks/{$block['block_link']}.php");
	}

	// CUSTOM BLOCKS
	else if ($block['block_link'] == NULL)
	{
		$show = 1;

		// this is to make sure the google ad only shows up for non ad-free people
		if ($block['nonpremium_only'] == 1)
		{
			if ($user->check_group([13]) === true)
			{
				$show = 0;
			}
		}

		if ($block['homepage_only'] == 1)
		{
			$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
			$check_url = parse_url($actual_link);
			if ($check_url['path'] != '/')
			{
				$show = 0;
			}
		}

		if ($show == 1)
		{
			$templating->load('blocks/block_custom');

			if ($block['style'] == 'block')
			{
				$templating->block('block');
			}
			if ($block['style'] == 'block_plain')
			{
				$templating->block('block_plain');
			}
			$title = '';
			// any title link?
			if (!empty($block['block_title_link']))
			{
				$title = "<a href=\"{$block['block_title_link']}\" target=\"_blank\">{$block['block_title']}</a>";
			}
			else if (!empty($block['block_title']))
			{
				$title = $block['block_title'];
			}

			$templating->set('block_title', $title);
			$templating->set('block_content', $block['block_custom_content']);
		}
	}
}


$templating->block('right_end', 'mainpage');

include(APP_ROOT . '/includes/footer.php');
?>
