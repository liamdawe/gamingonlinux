<?php
if(!defined('golapp')) 
{
	die('Direct access not permitted');
}
if (core::$current_module['module_file_name'] != 'guides')
{
    $templating->load('blocks/block_guides_latest');

    $latest_guides = $dbl->run("SELECT a.`article_id`, a.`author_id`, a.`comment_count`, a.`title`, a.`active`, a.`thumbnail_alt_text`, a.`date`, a.`slug`, a.`tagline_image`, a.`gallery_tagline`, a.`article_type`, g.`filename`, u.`username`, u.`profile_address` FROM `articles` a LEFT JOIN `articles_tagline_gallery` g ON g.`id` = a.`gallery_tagline` LEFT JOIN `users` u on a.`author_id` = u.`user_id` WHERE a.`active` = 1 AND a.`article_type` = 'guide' ORDER BY a.`date` DESC LIMIT 2")->fetch_all();

    if ($latest_guides)
    {
        $templating->block('guides');

        foreach ($latest_guides as $guide)
        {
            $thumbnail_alt_text = '';
            if (!empty($guide['thumbnail_alt_text']))
            {
                $thumbnail_alt_text = $guide['thumbnail_alt_text'];
            }

            if ($guide['gallery_tagline'] != 0)
            {
                $thumb_url = url . 'uploads/tagline_gallery/' . $guide['filename'];
            }
            else
            {
                $thumb_url = url . 'uploads/articles/tagline_images/'.$guide['tagline_image'];
            }

            $article_link = $article_class->article_link(array('date' => $guide['date'], 'slug' => $guide['slug'], 'type' => $guide['article_type']));

            $guides_list .= '<a href="'.$article_link.'"><img height="420" width="740" src="'.$thumb_url.'" alt="'.$thumbnail_alt_text.'">'.$guide['title'].'</a><br />'; 
        }
        $templating->set('guides_list', $guides_list);
    }
}