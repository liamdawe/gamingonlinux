<?php
if(!defined('golapp')) 
{
	die('Direct access not permitted: admin featured article config.');
}

$image_upload = new image_upload($dbl, $core);

$templating->load('admin_modules/admin_module_featured');

if (isset($_GET['view']))
{
	if ($_GET['view'] == 'add')
	{
		if (isset($_GET['article_id']) && is_numeric($_GET['article_id']))
		{
			$title = $dbl->run("SELECT `title`, `date`, `slug` FROM `articles` WHERE `article_id` = ?", array($_GET['article_id']))->fetch();
			if ($title)
			{
				$templating->block('add', 'admin_modules/admin_module_featured');

				$templating->set('article_title', $title['title']);
				$templating->set('article_id', $_GET['article_id']);
				$templating->set('article_link', $article_class->article_link(['slug' => $title['slug'], 'date' => $title['date']]));
			}
			else 
			{
				$core->message('Article does not exist!');
			}
		}
		else
		{
			$core->message("The article ID was not set or not numeric. This is likely a bug. Please report it.");
		}
	}

	if ($_GET['view'] == 'manage')
	{
		$templating->block('manage_top', 'admin_modules/admin_module_featured');
		$templating->set('total_featured', $core->config('total_featured'));

		$res = $dbl->run("SELECT p.`article_id`, p.end_date, a.`title`, a.`date`, a.`slug` FROM `editor_picks` p INNER JOIN `articles` a ON p.article_id = a.article_id ORDER BY p.`end_date` ASC")->fetch_all();

		if ($res)
		{
			foreach ($res as $items)
			{
				$templating->block('manage_item', 'admin_modules/admin_module_featured');
				$templating->set('title', $items['title']);
				$templating->set('article_link', $article_class->article_link($items));
				$templating->set('article_id', $items['article_id']);

				$end_date = new DateTime($items['end_date']);
				$templating->set('end_date', $end_date->format('Y-m-d'));
				$templating->set('end_time', $end_date->format('H:i:s'));
			}
		}
		else
		{
			$core->message("There are no current editor picks :( - you should probably add some!");
		}
	}
}

if (isset($_POST['act']))
{
	if ($_POST['act'] == 'add')
	{
		$end_date = $_POST['end_date'] . ' ' . $_POST['end_time'];

		if (!isset($_POST['article_id']) || (isset($_POST['article_id']) && !is_numeric($_POST['article_id'])))
		{
			$_SESSION['message'] = 'empty';
			$_SESSION['message_extra'] = 'article id';
			header("Location: /admin.php?module=featured&view=manage");
			die();
		}

		// make sure date is valid
		if (!core::validateDate(strtotime($end_date)))
		{
			$_SESSION['message'] = 'invalid_end_date';
			header("Location: /admin.php?module=featured&view=add&article_id=".$_POST['article_id']);
			die();
		}

		// make sure end date isn't before today
		if( strtotime($end_date) < strtotime('now') ) 
		{
			$_SESSION['message'] = 'end_date_wrong';
			header("Location: /admin.php?module=featured&view=add&article_id=".$_POST['article_id']);
			die();
		}

		$dbl->run("UPDATE `articles` SET `show_in_menu` = 1 WHERE `article_id` = ?", array($_POST['article_id']));

		$dbl->run("UPDATE `config` SET `data_value` = (data_value + 1) WHERE `data_key` = 'total_featured'");

		$end_date = $_POST['end_date'] . ' ' . $_POST['end_time'] . ':00';

		$dbl->run("INSERT INTO `editor_picks` SET `article_id` = ?, `end_date` = ?", array($_POST['article_id'], $end_date));

		// update cache
		$new_featured_total = $core->config('total_featured') + 1;
		$core->set_dbcache('CONFIG_total_featured', $new_featured_total); // no expiry as config hardly ever changes

		// note who deleted it
		$core->new_admin_note(array('completed' => 1, 'content' => ' added a new featured article.'));

		$_SESSION['message'] = 'added';
		header("Location: /admin.php?module=featured&view=manage");
		die();
	}

	if ($_POST['act'] == 'edit')
	{
		$end_date = $_POST['end_date'] . ' ' . $_POST['end_time'];

		if (!isset($_POST['article_id']) || (isset($_POST['article_id']) && !is_numeric($_POST['article_id'])))
		{
			$_SESSION['message'] = 'empty';
			$_SESSION['message_extra'] = 'article id';
			header("Location: /admin.php?module=featured&view=manage");
			die();
		}

		// make sure date is valid
		if (!core::validateDate(strtotime($end_date)))
		{
			$_SESSION['message'] = 'invalid_end_date';
			header("Location: /admin.php?module=featured&view=manage");
			die();
		}

		// make sure end date isn't before today
		if( strtotime($end_date) < strtotime('now') ) 
		{
			$_SESSION['message'] = 'end_date_wrong';
			header("Location: /admin.php?module=featured&view=manage");
			die();
		}

		$dbl->run("UPDATE `editor_picks` SET `end_date` = ? WHERE `article_id` = ?", array($end_date, $_POST['article_id']));

		if (isset($_FILES['new_image']) && $_FILES['new_image']['name'] != "")
		{
			$upload = $image_upload->featured_image($_POST['article_id'], 0);
		}
		if (!isset($_SESSION['message'])) // no error from the upload
		{
			$_SESSION['message'] = 'edited';
		}

		// note who deleted it
		$core->new_admin_note(array('completed' => 1, 'content' => ' updated a featured article.'));

		header("Location: /admin.php?module=featured&view=manage");
		die();
	}

	if ($_POST['act'] == 'delete')
	{
		$article_class->remove_editor_pick($_POST['article_id']);

		// note who deleted it
		$core->new_admin_note(array('completed' => 1, 'content' => ' removed an article from being featured.'));

		header("Location: /admin.php?module=featured&view=manage");
		die();
	}
}
