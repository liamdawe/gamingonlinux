<?php
if(!defined('golapp')) 
{
	die('Direct access not permitted: admin config');
}

define("POPULAR_COUNTER_DEFAULT", 1000);

if (!$user->check_group(1))
{
	$core->message("You do not have permission to access this page!");
}

else
{
	$templating->set_previous('title', 'Configuration', 1);
	$templating->set_previous('meta_description', 'Configuration', 1);

	if (!isset($_POST['Submit']))
	{
		$templating->load('admin_modules/config');

		$templating->block('main');
		$templating->set('form_url', $core->config('website_url'));

		if ($core->config('site_online') == 1)
		{
			$templating->set('online_button', '<button type="submit" name="GoOffline">Put Offline</button>');
		}
		else
		{
			$templating->set('online_button', '<button type="submit" name="GoOnline">Go Online</button>');
		}

		$templating->set('contact_email', $core->config('contact_email'));
		$templating->set('mailer_email', $core->config('mailer_email'));

		$templating->set('cookie_domain', $core->config('cookie_domain'));
		$templating->set('javascript_static', $core->config('javascript_static'));
		$templating->set('static_image_url', $core->config('static_image_url'));

		// set the default module
		$templating->set('default_module', $core->config('default_module'));

		// are users allowed to register?
		$allow_registrations_check = '';
		if ($core->config('allow_registrations') == 1)
		{
			$allow_registrations_check = 'checked';
		}
		$templating->set('register_check', $allow_registrations_check);

		$templating->set('reg_message', $core->config('register_off_message'));

		// is there a captcha on register?
		$register_captcha_check = '';
		if ($core->config('register_captcha') == 1)
		{
			$register_captcha_check = 'checked';
		}
		$templating->set('register_captcha_check', $register_captcha_check);
		
		$rss_check = '';
		if ($core->config('articles_rss') == 1)
		{
			$rss_check = 'checked';
		}
		$templating->set('article_rss_check', $rss_check);
		
		$forum_rss_check = '';
		if ($core->config('forum_rss') == 1)
		{
			$forum_rss_check = 'checked';
		}
		$templating->set('forum_rss_check', $forum_rss_check);

		$templating->set('popular_counter', $core->config('hot-article-viewcount'));

		$templating->set('mod_queue_minimum', $core->config('mod_queue_minimum_posts'));

		// debug mode on?
		$debug_check = '';
		if ($core->config('show_debug') == 1)
		{
			$debug_check = 'checked';
		}
		$templating->set('debug_check', $debug_check);

		// comments being moderated
		$all_comments_moderated = '';
		if ($core->config('all_comments_moderated') == 1)
		{
			$all_comments_moderated = 'checked';
		}
		$templating->set('all_comments_moderated_check', $all_comments_moderated);

		$comments_per_page = array(10, 20, 30, 40, 50);
		foreach ($comments_per_page as $total)
		{
			$selected = '';
			if ($total == $core->config('default-comments-per-page'))
			{
				$selected = 'selected';
			}
			$comments_per_page_options .= '<option value="'.$total.'" '.$selected.'>'.$total.'</option>';
		}
		$templating->set('comments_per_page_options', $comments_per_page_options);

		$templating->set('url', $core->config('website_url'));

		// META
		$templating->set('home_meta_title', $core->config('meta_homepage_title'));
		$templating->set('home_meta_desc', $core->config('meta_description'));
		
		// SOCIAL
		$templating->set('twitter', $core->config('twitter_username'));
		$templating->set('telegram_group', $core->config('telegram_group'));
		$templating->set('telegram_bot_key', $core->config('telegram_bot_key'));
		$templating->set('discord', $core->config('discord'));
		$templating->set('steam_group', $core->config('steam_group'));
		$templating->set('youtube_channel', $core->config('youtube_channel'));

		// THEMING
		$templating->set('template', $core->config('template'));

		$core->article_editor(['content' => $core->config('support_us_text')]);

		$templating->block('bottom', 'admin_modules/config');
	}

	// We have been asked to edit the config
	else if (isset($_POST['Submit']))
	{
		$template = trim($_POST['template']);
		$default_module = trim($_POST['default_module']);
		$home_title = core::make_safe($_POST['home_meta_title']);

		$allow_registrations = 0;
		if (isset($_POST['allow_registrations']))
		{
			$allow_registrations = 1;
		}

		$register_captcha = 0;
		if (isset($_POST['register_captcha']))
		{
			$register_captcha = 1;
		}
		
		$article_rss = 0;
		if (isset($_POST['article_rss']))
		{
			$article_rss = 1;
		}
		
		$forum_rss = 0;
		if (isset($_POST['forum_rss']))
		{
			$forum_rss = 1;
		}

		$debug = 0;
		if (isset($_POST['debug']))
		{
			$debug = 1;
		}

		$moderated = 0;
		if (isset($_POST['all_comments_moderated']))
		{
			$moderated = 1;
		}

		if (!is_numeric($_POST['default_comments_per_page']))
		{
			$default_comments_per_page = 10;
		}
		else
		{
			$default_comments_per_page = $_POST['default_comments_per_page'];
		}

		if (!is_numeric($_POST['mod_queue_minimum']))
		{
			$mod_queue_minimum = 5;
		}
		else
		{
			$mod_queue_minimum = $_POST['mod_queue_minimum'];
		}

		$popular_counter = POPULAR_COUNTER_DEFAULT;
		if (isset($_POST['popular_counter']) && is_numeric($_POST['popular_counter']))
		{
			$popular_counter = $_POST['popular_counter'];
		}

		$empty_check = core::mempty(compact('template', 'default_module', 'home_title'));
		if ($empty_check !== true)
		{
			$_SESSION['message'] = 'empty';
			$_SESSION['message_extra'] = $empty_check;
			header("Location: " . url . "/admin.php?module=config");
			die();
		}

		// do the update
		else
		{
			$core->set_config($_POST['contact_email'], 'contact_email');

			$core->set_config($_POST['cookie_domain'], 'cookie_domain');

			$core->set_config($_POST['javascript_static'], 'javascript_static');

			$core->set_config($_POST['static_image_url'], 'static_image_url');

			$core->set_config($default_module, 'default_module');

			$core->set_config($allow_registrations, 'allow_registrations');

			$core->set_config($_POST['reg_message'], 'register_off_message');

			$core->set_config($register_captcha, 'register_captcha');

			$core->set_config($_POST['url'], 'website_url');

			$core->set_config($popular_counter, 'hot-article-viewcount');

			$core->set_config($debug, 'show_debug');

			$core->set_config($moderated, 'all_comments_moderated');
			
			$core->set_config($article_rss, 'articles_rss');
			
			$core->set_config($forum_rss, 'forum_rss');

			$core->set_config($default_comments_per_page, 'default-comments-per-page');

			$core->set_config($mod_queue_minimum, 'mod_queue_minimum_posts');
			
			$core->set_config($_POST['mailer_email'], 'mailer_email');

			// META
			$core->set_config($home_title, 'meta_homepage_title');
			$core->set_config($_POST['home_meta_desc'], 'meta_description');
			
			// SOCIAL
			$core->set_config($_POST['twitter'], 'twitter_username');
			$core->set_config($_POST['telegram_group'], 'telegram_group');
			$core->set_config($_POST['telegram_bot_key'], 'telegram_bot_key');
			$core->set_config($_POST['discord'], 'discord');
			$core->set_config($_POST['steam_group'], 'steam_group');
			$core->set_config($_POST['youtube_channel'], 'youtube_channel');
			
			// THEMING
			$core->set_config($template, 'template');

			$core->set_config($_POST['text'], 'support_us_text');

			// note who did it
			$core->new_admin_note(array('completed' => 1, 'content' => ' updated the website config.'));

			$_SESSION['message'] = 'edited';
			$_SESSION['message_extra'] = 'config';
			header('Location: '.$core->config('website_url').'admin.php?module=config');
			die();
		}
	}
	if (isset($_POST['GoOffline']))
	{
		$core->set_config(0, 'site_online');

		// note who did it
		$core->new_admin_note(array('completed' => 1, 'content' => ' put the site offline.'));
		
		header('Location: '.$core->config('website_url').'admin.php?module=config');
		die();
	}
	if (isset($_POST['GoOnline']))
	{
		$core->set_config(1, 'site_online');

		// note who did it
		$core->new_admin_note(array('completed' => 1, 'content' => ' put the site online.'));

		header('Location: '.$core->config('website_url').'admin.php?module=config');
		die();
	}
}
?>