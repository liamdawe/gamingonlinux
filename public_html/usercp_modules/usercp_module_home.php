<?php
if(!defined('golapp')) 
{
	die('Direct access not permitted');
}
define('articles_per_page_max', 50);
define('per_page_max', 100); // comments and posts

$templating->set_previous('title', 'Home' . $templating->get('title', 1)  , 1);
$templating->load('usercp_modules/usercp_module_home');

if (isset($_GET['blocktag']) && isset($_GET['tagid']) && is_numeric($_GET['tagid']))
{
	// check it's not blocked already
	$check = $dbl->run("SELECT `ref_id` FROM `user_tags_bar` WHERE `user_id` = ? AND `category_id` = ?", array($_SESSION['user_id'], $_GET['tagid']))->fetch();

	if (!$check)
	{
		$dbl->run("INSERT INTO `user_tags_bar` SET `user_id` = ?, `category_id` = ?", array($_SESSION['user_id'], $_GET['tagid']));
		$_SESSION['message'] = 'saved';
		$_SESSION['message_extra'] = 'set of excluded article tags';
		header("Location: " . $core->config('website_url') . "usercp.php");
	}
}

if (isset($_GET['unblocktag']) && isset($_GET['tagid']) && is_numeric($_GET['tagid']))
{
	// check it's actually blocked already
	$check = $dbl->run("SELECT `ref_id` FROM `user_tags_bar` WHERE `user_id` = ? AND `category_id` = ?", array($_SESSION['user_id'], $_GET['tagid']))->fetch();

	if ($check)
	{
		$dbl->run("DELETE FROM `user_tags_bar` WHERE  `user_id` = ? AND `category_id` = ?", array($_SESSION['user_id'], $_GET['tagid']));
		$_SESSION['message'] = 'saved';
		$_SESSION['message_extra'] = 'set of excluded article tags';
		header("Location: " . $core->config('website_url') . "usercp.php");
	}
}

if (!isset($_POST['act']))
{
	$usercpcp = $dbl->run("SELECT `submission_emails`, `single_article_page`, `per-page`, `articles-per-page`, `theme`, `timezone`, `email_articles`, `mailing_list_key`, `supporter_end_date`, `supporter_type`, `hide_adverts`, `full_rss_key`, `affiliate_links` FROM `users` WHERE `user_id` = ?", array($_SESSION['user_id']))->fetch();
	
	// make sure they have a mailing_list_key
	// if they unsubscribe it's wiped, but if they stay subscribed/make a new sub = use new or existing key
	$mail_list_key = $usercpcp['mailing_list_key'];
	if (empty($usercpcp['mailing_list_key']) || $usercpcp['mailing_list_key'] = NULL)
	{
		$unsub_key = core::random_id();
		$mail_list_key = $unsub_key;

		$dbl->run("UPDATE `users` SET `mailing_list_key` = ? WHERE `user_id` = ?", array($unsub_key, $_SESSION['user_id']));
	}
	$templating->set('unsub_key', $mail_list_key);

	$templating->block('top', 'usercp_modules/usercp_module_home');

	if ($user->can('premium_features'))
	{
		if (empty($usercpcp['full_rss_key']) || $usercpcp['full_rss_key'] == NULL)
		{
			$user_rss_key = bin2hex(random_bytes(32));
			$dbl->run("UPDATE `users` SET `full_rss_key` = ? WHERE `user_id` = ?", array($user_rss_key, $_SESSION['user_id']));
		}
		else
		{
			$user_rss_key = $usercpcp['full_rss_key'];
		}

		$templating->block('premium', 'usercp_modules/usercp_module_home');

		$templating->set("rss_feed_link", $core->config('website_url') . 'special_rss.php?code=' . $user_rss_key);

		$end_date = '';
		if ($user->check_group(6))
		{
			if ($usercpcp['supporter_end_date'] != NULL)
			{
				$end_date = '<p>Supporter status end date: '.$usercpcp['supporter_end_date'].'</p>';
			}
		}

		$forum_link = '';
		if ($usercpcp['supporter_type'] == 'patreon')
		{
			$forum_link = '<p><a href="https://www.gamingonlinux.com/forum/supporters-only/">Patreon Supporter-only forum link</a></p>';
		}

		$templating->set('forum_link', $forum_link);
		$templating->set('end_date_info', $end_date);

		$adverts_checker = '';
		if ($usercpcp['hide_adverts'] == 1)
		{
			$adverts_checker = 'checked';
		}
		$templating->set('hide_adverts_checker', $adverts_checker);

		$affiliate_links_output = '';
		// If higher level
		if ($user->check_group([9,16,1]))
		{
			$affiliate_checked = '';
			if ($usercpcp['affiliate_links'] === 0)
			{
				$affiliate_checked = 'checked';
			}
			$affiliate_links_output = '<label for="hide_affiliates"> Hide affiliate links? <input type="checkbox" name="hide_affiliates" '.$affiliate_checked.'/></label><br />
			<em>Affiliate links are essential to keep GamingOnLinux funded. However, we understand many plugins block them and some don\'t even open the link. So as a higher-level supporter, you can have them turned off.</em><br />
			<br />';
		}
		$templating->set('affiliate_links', $affiliate_links_output);

		$templating->set('csrf', $_SESSION['csrf_token']);
	}
	else
	{
		$templating->block('no_premium', 'usercp_modules/usercp_module_home');
	}

	$templating->block('main', 'usercp_modules/usercp_module_home');
	$templating->set('url', $core->config('website_url'));
	$templating->set('csrf', $_SESSION['csrf_token']);

	/* for content preferences */
	// grab a list of tags they don't want on the homepage
	$tags_list = '';
	$user_tag_bars = $dbl->run("SELECT ut.`category_id`, c.`category_name` FROM `user_tags_bar` ut LEFT JOIN `articles_categorys` c ON ut.category_id = c.category_id WHERE ut.`user_id` = ?", array($_SESSION['user_id']))->fetch_all();
	foreach ($user_tag_bars as $tags)
	{
		$tags_list .= "<option value=\"{$tags['category_id']}\" selected>{$tags['category_name']}</option>";
	}
	$templating->set('tags_list', $tags_list);

	$theme_options = '';
	$theme_list = ['', 'light', 'dark'];

	foreach ($theme_list as $theme)
	{
		$selected = '';
		if ($usercpcp['theme'] == $theme)
		{
			$selected = 'selected';
		}
		
		$theme_options .= '<option value="'.$theme.'" '.$selected.'>'.$theme.'</option>';
	}

	$templating->set('theme_options', $theme_options);
	
	$templating->set('timezone_list', core::timezone_list($usercpcp['timezone']));

	$submission_emails = '';
	if ($user->check_group([1,2,5]) == true)
	{
		$submission_emails_check = '';
		if ($usercpcp['submission_emails'] == 1)
		{
			$submission_emails_check = 'checked';
		}
		$submission_emails = "Get article submission emails? <input type=\"checkbox\" name=\"submission_emails\" $submission_emails_check /><br />";
	}
	$templating->set('submission_emails', $submission_emails);
	
	$daily_article_emails = '';
	if ($usercpcp['email_articles'] == 'daily')
	{
		$daily_article_emails = 'checked';
	}
	$templating->set('daily_check', $daily_article_emails);

	$single_article_yes = '';
	if ($usercpcp['single_article_page'] == 1)
	{
		$single_article_yes = 'selected';
	}
	$templating->set('single_article_yes', $single_article_yes);

	$single_article_no = '';
	if ($usercpcp['single_article_page'] == 0)
	{
		$single_article_no = 'selected';
	}
	$templating->set('single_article_no', $single_article_no);

	$page_options = '';
	$per_page_selected = '';
	for ($i = 30; $i <= per_page_max; $i += 10)
	{
		if ($i == $usercpcp['per-page'])
		{
			$per_page_selected = 'selected';
		}
		$page_options .= '<option value="'.$i.'" '.$per_page_selected.'>'.$i.'</a>';
		$per_page_selected = '';
	}
	$templating->set('per-page', $page_options);

	$apage_options = '';
	$aper_page_selected = '';
	for ($i = 15; $i <= articles_per_page_max; $i += 5)
	{
		if ($i == $usercpcp['articles-per-page'])
		{
			$aper_page_selected = 'selected';
		}
		$apage_options .= '<option value="'.$i.'" '.$aper_page_selected.'>'.$i.'</a>';
		$aper_page_selected = '';
	}
	$templating->set('aper-page', $apage_options);
}

else if (isset($_POST['act']))
{
	if (!isset($_POST['csrf']))
	{
		die('Security token not set! If this is a legitimate request, please report the bug.');			
	}
	if (!hash_equals($_SESSION['csrf_token'], $_POST['csrf'])) 
	{
		die('Security token not valid! If this is a legitimate request, please report the bug.');
	}

	if ($_POST['act'] == 'update_premium')
	{
		$hide_adverts = 0;
		if (isset($_POST['hide_adverts']))
		{
			$hide_adverts = 1;
		}

		$affiliate_links = 1;
		if ($user->check_group([9,16,1]))
		{
			
			if (isset($_POST['hide_affiliates']))
			{
				$affiliate_links = 0;
			}
			else
			{
				$affiliate_links = 1;
			}		
		}

		$dbl->run("UPDATE `users` SET `hide_adverts` = ?, `affiliate_links` = ? WHERE `user_id` = ?", array($hide_adverts, $affiliate_links, $_SESSION['user_id']));

		$_SESSION['hide_adverts'] = $hide_adverts;
		$_SESSION['affiliate_links'] = $affiliate_links;

		$_SESSION['message'] = 'profile_updated';
		header("Location: " . $core->config('website_url') . "usercp.php?module=home");
	}
	
	if ($_POST['act'] == 'Update')
	{
		$per_page = 10;
		if (is_numeric($_POST['per-page']))
		{
			if ($_POST['per-page'] > per_page_max)
			{
				$per_page = per_page_max;
			}
			else
			{
				$per_page = $_POST['per-page'];
			}
		}

		$aper_page = 15;
		if (is_numeric($_POST['articles-per-page']))
		{
			if ($_POST['articles-per-page'] > articles_per_page_max)
			{
				$aper_page = articles_per_page_max;
			}
			else
			{
				$aper_page = $_POST['articles-per-page'];
			}
		}

		$single_article_page = 0;
		if ($_POST['single_article_page'] == 1 || $_POST['single_article_page'] == 0)
		{
			$single_article_page = $_POST['single_article_page'];
		}

		$submission_emails = 0;
		if ($user->check_group([1,2,5]) == true)
		{
			if (isset($_POST['submission_emails']))
			{
				$submission_emails = 1;
			}
		}
		
		$daily_articles = NULL;
		$mailing_list_key = NULL;
		if (isset($_POST['daily_news']))
		{
			$daily_articles = 'daily';
			$mailing_list_key = $_POST['mailing_list_key'];
		}

		$dbl->run("UPDATE `users` SET 
		`email_articles` = ?, 
		`submission_emails` = ?, 
		`single_article_page` = ?, 
		`articles-per-page` = ?, 
		`per-page` = ?, 
		`timezone` = ?, 
		`theme` = ? WHERE `user_id` = ?", array($daily_articles, $submission_emails, $single_article_page, $aper_page, $per_page, $_POST['timezone'], $_POST['theme'], $_SESSION['user_id']));

		$_SESSION['per-page'] = $per_page;
		$_SESSION['articles-per-page'] = $aper_page;

		$_SESSION['message'] = 'profile_updated';
		header("Location: " . $core->config('website_url') . "usercp.php?module=home");
	}

	if ($_POST['act'] == 'bar_tags')
	{
		if (isset($_POST['bar_tags']) && !empty($_POST['bar_tags']))
		{
			// delete any existing categories that aren't in the final list for publishing
			$user_tag_bars = $dbl->run("SELECT ut.`ref_id`, ut.`category_id`, c.`category_name` FROM `user_tags_bar` ut LEFT JOIN `articles_categorys` c ON ut.category_id = c.category_id WHERE ut.`user_id` = ?", array($_SESSION['user_id']))->fetch_all();

			if (!empty($user_tag_bars))
			{
				foreach ($user_tag_bars as $tag)
				{
					if (!in_array($tag['category_id'], $_POST['bar_tags']))
					{
						$dbl->run("DELETE FROM `user_tags_bar` WHERE `ref_id` = ?", array($tag['ref_id']));
					}
				}
			}

			// get fresh list of categories, and insert any that don't exist
			$current_tags = $dbl->run("SELECT `category_id` FROM `user_tags_bar` WHERE `user_id` = ?", array($_SESSION['user_id']))->fetch_all(PDO::FETCH_COLUMN, 0);

			foreach($_POST['bar_tags'] as $new_tag)
			{
				if (!in_array($new_tag, $current_tags))
				{
					$dbl->run("INSERT INTO `user_tags_bar` SET `user_id` = ?, `category_id` = ?", array($_SESSION['user_id'], $new_tag));
				}
			}
		}
		if ((isset($_POST['bar_tags']) && empty($_POST['bar_tags']) || !isset($_POST['bar_tags'])))
		{
			$dbl->run("DELETE FROM `user_tags_bar` WHERE `user_id` = ?", array($_SESSION['user_id']));
		}

		$_SESSION['message'] = 'saved';
		$_SESSION['message_extra'] = 'set of excluded article tags';
		header("Location: " . $core->config('website_url') . "usercp.php");
	}
}
?>
