<?php
if(!defined('golapp')) 
{
	die('Direct access not permitted');
}
$templating->set_previous('title', 'Personal Profile Details' . $templating->get('title', 1)  , 1);
$templating->load('usercp_modules/profile_details');

include('includes/profile_fields.php');

$templating->block('main', 'usercp_modules/profile_details');

$db_grab_fields = '';
foreach ($profile_fields as $field)
{
	$db_grab_fields .= "`{$field['db_field']}`, ";
}

$current_details = $dbl->run("SELECT $db_grab_fields `profile_address`, `article_bio`, `about_me`, `fediverse_tag`, `twitter_username` FROM `users` WHERE `user_id` = ?", array($_SESSION['user_id']))->fetch();

$profile_address = '';
if (!empty($current_details['profile_address']))
{
    $profile_address = $current_details['profile_address'];
}
$templating->set('profile_address', $profile_address);

$about_me = '';
if (!empty($current_details['about_me']))
{
    $about_me = $current_details['about_me'];
}
$templating->set('about_me', $current_details['about_me']);

$article_bio = '';
if (!empty($current_details['article_bio']))
{
    $article_bio = $current_details['article_bio'];
}
$templating->set('article_bio', $current_details['article_bio']);
$templating->set('fediverse_tag', $current_details['fediverse_tag']);

$profile_fields_output = '';

$profile_fields_total = count($profile_fields);
$profile_fields_current = 0;

foreach ($profile_fields as $field)
{
	$profile_fields_current++;

	if (!empty($current_details[$field['db_field']]))
	{
		$current_details[$field['db_field']] = htmlspecialchars($current_details[$field['db_field']]);
	}
	
	$url = '';
	if ($field['base_link_required'] == 1)
	{
		$url = $field['base_link'];
	}

	$span = '';
	if (isset($field['span']))
	{
		$span = $field['span'];
	}

	$description = '';
	if (isset($field['description']))
	{
		$description = ' - ' . $field['description'];
	}

	$form_input = "";
	$preinput = 0;
	if (isset($field['preinput']) && $field['preinput'] != NULL)
	{
		$preinput = 1;
		$form_input  .= '<div class="input-field"><span class="addon">'.$field['preinput'].'</span>';
	}
	else
	{
		$form_input .= "<div style=\"display:inline;\">";
	}
	
	$form_input .= "<input id=\"{$field['db_field']}_field\" type=\"text\" name=\"{$field['db_field']}\" value=\"{$current_details[$field['db_field']]}\" />";
	$form_input .= "</div>";

	$profile_fields_output .= "<label for=\"{$field['name']}\"> $span {$field['name']} $form_input <small>$description</small></label><br />";

	if ($profile_fields_current != $profile_fields_total)
	{
		$profile_fields_output .= '<br />';
	}
}

$templating->set('profile_fields', $profile_fields_output);

$templating->set('csrf', $_SESSION['csrf_token']);

if (isset($_POST['act']))
{
    if ($_POST['act'] == 'update')
    {
		if (!isset($_POST['csrf']))
		{
			die('Security token not set! If this is a legitimate request, please report the bug.');			
		}
		if (!hash_equals($_SESSION['csrf_token'], $_POST['csrf'])) 
		{
			die('Security token not valid! If this is a legitimate request, please report the bug.');
		}

        $_POST['profile_address'] = trim($_POST['profile_address']);

		if (!empty($_POST['profile_address']))
		{
			// check for naughtyness
			$not_allowed = array('admin','gamingonlinux','moderator','owner','mod');
			foreach($not_allowed as $string)
			{
				if(strpos($_POST['profile_address'], $string) !== false) 
				{
					$_SESSION['message'] = 'naughty';
					header("Location: /usercp.php?module=profile");
					die();    
				}
			}

			// check it's a correct value
			$aValid = array('-', '_');
			if(!ctype_alnum(str_replace($aValid, '', $_POST['profile_address'])))
			{
				$_SESSION['message'] = 'url-characters';
				header("Location: /usercp.php?module=profile");
				die();
			}
			
			if (strlen($_POST['profile_address']) < 4)
			{
				$_SESSION['message'] = 'url-too-short';
				header("Location: /usercp.php?module=profile");
				die();
			}
		}
		else
		{
			$_POST['profile_address'] = NULL;
		}
		
		$_POST['about_me'] = core::make_safe($_POST['about_me'], ENT_QUOTES);
		$_POST['article_bio'] = core::make_safe($_POST['article_bio'], ENT_QUOTES);
		$_POST['fediverse_tag'] = core::make_safe($_POST['fediverse_tag'], ENT_QUOTES);

        if ($_POST['profile_address'] != $current_details['profile_address'])
        {
            // check for duplicates
            $checker = $dbl->run("SELECT `user_id` FROM `users` WHERE `profile_address` = ?", array($_POST['profile_address']))->fetch();
            if ($checker)
            {
                $_SESSION['message'] = 'exists';
                header("Location: /usercp.php?module=profile");
                die();
            }
		}
		
		// save
		$dbl->run("UPDATE `users` SET `profile_address` = ?, `article_bio` = ?, `about_me` = ?, `fediverse_tag` = ? WHERE `user_id` = ?", array($_POST['profile_address'], $_POST['article_bio'], $_POST['about_me'], $_POST['fediverse_tag'], $_SESSION['user_id']));

		foreach ($profile_fields as $key => $field)
		{
			// tell them to do it properly
			if ($field['db_field'] == 'youtube' && (!empty($_POST['youtube']) && strpos($_POST['youtube'], "youtube.com") === false))
			{
				$_SESSION['message'] = 'youtube-missing';
				header("Location: " . $core->config('website_url') . "usercp.php?module=profile");
				die();
			}

			// tell them to do it properly
			if ($field['db_field'] == 'twitch' && (!empty($_POST['twitch']) && strpos($_POST['twitch'], "twitch.tv") === false))
			{
				$_SESSION['message'] = 'twitch-missing';
				header("Location: " . $core->config('website_url') . "usercp.php?module=profile");
				die();
			}

			if ($field['plain_link'] == 1 && !empty($_POST[$key]) && (strpos($_POST[$key], 'https://') === false && strpos($_POST[$key], 'http://') === false))
			{
				$_SESSION['message'] = 'broken_link';
				$_SESSION['message_extra'] = $key;
				header("Location: " . $core->config('website_url') . "usercp.php?module=profile");
				die();
			}

			else
			{
				$sanatized = trim(strip_tags($_POST[$field['db_field']]));

				if (!empty($sanatized))
				{
					$check_link = $sanatized;
					if ($field['base_link_required'] == 1)
					{
						$check_link = $field['base_link'] . $sanatized;
					}

					// make doubly sure it's an actual URL
					if (filter_var($check_link, FILTER_VALIDATE_URL))
					{
						$dbl->run("UPDATE `users` SET `{$field['db_field']}` = ? WHERE `user_id` = ?", array($sanatized, $_SESSION['user_id']));
					}
				}
				else
				{
					$dbl->run("UPDATE `users` SET `{$field['db_field']}` = NULL WHERE `user_id` = ?", array($_SESSION['user_id']));
				}
			}
		}

		$core->delete_dbcache('author_info_'.$_SESSION['user_id']);

        $_SESSION['message'] = 'profile_updated';
        header("Location: /usercp.php?module=profile");
        die();
    }
}
