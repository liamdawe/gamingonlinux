<?php
session_start();

define("APP_ROOT", dirname ( dirname ( dirname(__FILE__) ) ) );

require APP_ROOT . "/includes/bootstrap.php";

$cat_array = array();

if(isset($_GET['block']))
{
	// get mod ids so people can't get around moderation by blocking
	$get_mods = $dbl->run("SELECT `user_id` FROM `user_group_membership` WHERE `group_id` IN (1,2,5)")->fetch_all(PDO::FETCH_COLUMN, 0);

	$in  = str_repeat('?,', count($get_mods) - 1) . '?';

	$username_search = ['%' . $_GET['q'] . '%'];
	$get_data = $dbl->run("SELECT `user_id`, `username` FROM `users` WHERE `user_id` NOT IN ($in) AND `username` LIKE ? ORDER BY `username` ASC", array_merge($get_mods, $username_search))->fetch_all();

	// Make sure we have a result
	if($get_data)
	{
		foreach ($get_data as $key => $value)
		{
			$data[] = array('id' => $value['user_id'], 'text' => $value['username']);
		}
	}
	else
	{
		$data[] = array('text' => 'No users found that match!');
	}
	echo json_encode($data);	
}
else if(isset($_GET['q']))
{
	$username_search = '%' . $_GET['q'] . '%';
	$get_data = $dbl->run("SELECT `user_id`, `username` FROM `users` WHERE `username` LIKE ? ORDER BY `username` ASC", [$username_search])->fetch_all();

	// Make sure we have a result
	if($get_data)
	{
		foreach ($get_data as $key => $value)
		{
			$data[] = array('id' => $value['user_id'], 'text' => $value['username']);
		}
	}
	else
	{
		$data[] = array('text' => 'No users found that match!');
	}
	echo json_encode($data);
}


?>
