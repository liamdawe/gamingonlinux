<?php
ini_set("memory_limit","50M");

session_start();

define("APP_ROOT", dirname ( dirname ( dirname ( dirname(__FILE__) ) ) ) );

require APP_ROOT . "/includes/bootstrap.php";

$key = $core->config('do_space_key_uploads');
$secret = $core->config('do_space_key_private_uploads');

use Aws\S3\S3Client;

$client = new Aws\S3\S3Client([
        'version' => 'latest',
        'region'  => 'am3',
        'endpoint' => 'https://ams3.digitaloceanspaces.com',
        'credentials' => [
                'key'    => $key,
                'secret' => $secret,
            ],
]);

use claviska\SimpleImage;
$img = new SimpleImage();

// only logged in accounts can do this too, it's only for articles
if (!isset($_SESSION['user_id']) || $_SESSION['user_id'] == 0)
{
	die('You shouldn\'t be here.');
}

if (strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest' )
{
	die('You shouldn\'t be here. Not an AJAX request.');
}

header('Content-Type: application/json');

define ("MAX_SIZE",50*1024*1024); // 50MB
function getExtension($str)
{
	$i = strrpos($str,".");
	if (!$i) { return ""; }
	$l = strlen($str) - $i;
	$ext = substr($str,$i+1,$l);
	return $ext;
}

$upload_max_size = ini_get('upload_max_filesize');

$return_data = [];
$valid_formats = array("jpg", "png", "gif", "jpeg", "svg", 'mp4', 'webm', 'ogg', 'mp3', 'webp');
$valid_formats_tagline = array("jpg", "png", "gif", "jpeg", "webp");

if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST")
{
	if (!isset($_POST['type']))
	{
		echo '<div class="box error"><div class="head">Error</div><div class="body group">Type of file upload was not selected. This is likely a bug.</div></div>';
		die();
	}

	if ($_POST['type'] == 'tagline')
	{
		//get the extension of the file in a lower case format
		$ext = getExtension($_FILES['photos2']['name']);
		$ext = strtolower($ext);

		if (isset($_FILES['photos2']['error']) && $_FILES['photos2']['error'] > 0)
		{
			$error_extra = '';
			if ($_FILES['photos2']['error'] == 1)
			{
				$error_extra = '';
			}
			echo '<div class="box error"><div class="head">Error</div><div class="body group">' . core::$phpFileUploadErrors[$_FILES['photos2']['error']] . '</div></div>';
			die();
		}
			
		if(in_array($ext,$valid_formats_tagline))
		{
			if (!@fopen($_FILES['photos2']['tmp_name'], 'r'))
			{
				echo '<span class="imgList">Did you select an image? Couldn\'t find one.</span>';
			}

			else
			{
				// make sure it's actually an image, for sure
				if (!getimagesize($_FILES['photos2']['tmp_name']))
				{
					echo '<span class="imgList">That was not an image!</span>';
					die();
				}
				
				// load the file
				$img->fromFile($_FILES['photos2']['tmp_name']);
				
				/* SCALING
				// Has to be at least 550 to work on social media websites for the image to be auto-included in posts like on G+ and Facebook
				// Do this by itself first, so that we can try to preserve aspect ratio!
				*/
				if ($img->getWidth() != 740 || $img->getHeight() != 420)
				{
					if ( $img->getMimeType() == 'image/jpeg' || $img->getMimeType() == 'image/png' || $img->getMimeType() == 'image/webp')
					{
						$img->resize(740, 420)->toFile($_FILES['photos2']['tmp_name']);
					}

					// don't mess with the gif man
					else if ( $img->getMimeType() == 'image/gif' )
					{
						echo '<span class="imgList">That gif is too small</span>';
						return;
					}

					clearstatcache();
				}

				/* CHECK FILE SIZE
				So we now know for sure it has the correct minimum dimensions, but is the filesize okay?
				*/
				if (filesize($_FILES['photos2']['tmp_name']) > $core->config('max_tagline_image_filesize'))
				{
					// okay, so width is good, let's try compressing directly instead then
					if ($img->getWidth() == 740)
					{
						if( $img->getMimeType() == 'image/jpeg' || $img->getMimeType() == 'image/png' )
						{
							$img->toFile($_FILES['photos2']['tmp_name'], NULL, 80);
						}

						// cannot compress gifs so it's just too big
						else if( $img->getMimeType() == 'image/gif' )
						{
							echo '<span class="imgList">File size too big!</span>';
							return;
						}
						
						clearstatcache();

						// if it's still too big, we don't want to compress any further or it will look bad
						if (filesize($_FILES['photos2']['tmp_name']) > $core->config('max_tagline_image_filesize'))
						{
							echo '<span class="imgList">File size too big, tried compressing it, but still too big!</span>';
							return;
						}
					}
				}
			}

			$file_ext = '';
			if( $img->getMimeType() == 'image/jpeg' )
			{
				$file_ext = 'jpg';
			}

			else if( $img->getMimeType() == 'image/gif' )
			{
				$file_ext = 'gif';
			}

			else if( $img->getMimeType() == 'image/png' )
			{
				$file_ext = 'png';
			}

			else if( $img->getMimeType() == 'image/webp' )
			{
				$file_ext = 'webp';
			}

			// give the image a random file name
			$imagename = rand() . 'idgol.' . $file_ext;

			// the actual image
			$source = $_FILES['photos2']['tmp_name'];

			// where to upload to
			$target = $core->config('path') . "uploads/articles/tagline_images/temp/" . $imagename;

			// make the thumbnail, nice and small
			$img->fromFile($_FILES['photos2']['tmp_name'])->resize(350, null)->toFile($core->config('path') . "uploads/articles/tagline_images/temp/thumbnails/" . $imagename);
			
			if (move_uploaded_file($source, $target))
			{
				// replace any existing just-uploaded image
				if (isset($_SESSION['uploads_tagline']))
				{
					unlink($core->config('path') . "uploads/articles/tagline_images/temp/" . $_SESSION['uploads_tagline']['image_name']);
					unlink($core->config('path') . "uploads/articles/tagline_images/temp/thumbnails/" . $_SESSION['uploads_tagline']['image_name']);
				}

				$_SESSION['uploads_tagline']['image_name'] = $imagename;
				$_SESSION['uploads_tagline']['image_rand'] = $_SESSION['image_rand'];

				// this will replace any previously selected gallery image
				unset($_SESSION['gallery_tagline_id']);
				unset($_SESSION['gallery_tagline_rand']);
				unset($_SESSION['gallery_tagline_filename']);

				$html_output = '<div class="test" id="'.$imagename.'"><img src="'.$core->config('website_url').'uploads/articles/tagline_images/temp/'.$imagename.'"><br />
				<input type="hidden" name="image_name" value="'.$imagename.'" />
				Full Image Url: <a class="tagline-image" href="'. $core->config('website_url') . 'uploads/articles/tagline_images/temp/'.$imagename.'" target="_blank">Click Me</a> - <a href="#" id="'.$imagename.'" class="trash_tagline">Delete Image</a>
				<br /><button type="button" class="insert_tagline_image">Insert into editor</button></div>';

				echo $html_output;
				return;
			}

			else
			{
				echo '<div class="box error"><div class="head">Error</div><div class="body group">There was an error while trying to upload!</div></div>';
				die();
			}
		}
		else 
		{
			echo '<div class="box error"><div class="head">Error</div><div class="body group">Unknown extension or extension not allowed!</div></div>';
			die();
		}
	}

	if ($_POST['type'] == 'article_media')
	{
		if (!isset($_POST['csrf']))
		{
			echo 'Security token not set! If this is a legitimate request, please report the bug.';
			die('Security token not set! If this is a legitimate request, please report the bug.');	
			return;		
		}
		if (!hash_equals($_SESSION['csrf_token'], $_POST['csrf'])) 
		{
			echo 'Security token not set! If this is a legitimate request, please report the bug.';
			die('Security token not valid! If this is a legitimate request, please report the bug.');
			return;
		}

		$uploaddir = $_SERVER['DOCUMENT_ROOT'] . "/uploads/articles/article_media/";
		$thumbs_dir = $_SERVER['DOCUMENT_ROOT'] . "/uploads/articles/article_media/thumbs/";
		foreach ($_FILES['media']['name'] as $name => $value)
		{
			$filename = stripslashes($_FILES['media']['name'][$name]);
			$size = filesize($_FILES['media']['tmp_name'][$name]);
			//get the extension of the file in a lower case format
			$ext = getExtension($filename);
			$ext = strtolower($ext);

			if (isset($_FILES['media']['error']) && $_FILES['media']['error'][0] > 0)
			{
				$error_extra = '';
				if ($_FILES['media']['error'][0] == 1)
				{
					$error_extra = '';
				}
				echo '<div class="box error"><div class="head">Error</div><div class="body group">' . core::$phpFileUploadErrors[$_FILES['media']['error'][0]] . '</div></div>';
				return;
				die();
			}

			if(in_array($ext,$valid_formats))
			{
				if ($size < (MAX_SIZE))
				{
					$new_media_name = rand().time().'gol'.$_SESSION['user_id'];
					$image_name = $new_media_name.'.'.$ext;
					$main_newname = $uploaddir.$image_name;

					$mime = mime_content_type($_FILES['media']['tmp_name'][$name]);

					if (isset($_POST['local_upload']))
					{
						$main_url = $core->config('website_url') . 'uploads/articles/article_media/' . $image_name;
						$location = NULL;
					}
					else
					{
						$main_url = $core->config('external_media_upload_url') . 'uploads/articles/article_media/' . $image_name;
						$location = $core->config('external_media_upload_url');
					}
					$gif_static_button = '';
					$thumbnail_button = '';
					$data_type = '';

					// only for images
					if ($ext != 'mp4' && $ext != 'webm' && $ext != 'ogg' && $ext != 'mp3')
					{
						// thumbs
						$img->fromFile($_FILES['media']['tmp_name'][$name]);

						// if resize selected
						if (isset($_POST['resize']))
						{
							$img->fromFile($_FILES['media']['tmp_name'][$name])->resize(930, null)->toFile($_FILES['media']['tmp_name'][$name]);
						}

						$thumb_newname = $thumbs_dir.$image_name;

						/* LOCAL FILESYSTEM UPLOADS */
						if (isset($_POST['local_upload']))
						{
							$thumb_url = $core->config('website_url') . 'uploads/articles/article_media/thumbs/' . $image_name;

							// so we don't make a big thumbnail of a small image
							if ($img->getWidth() <= 450)
							{
								$img->fromFile($_FILES['media']['tmp_name'][$name])->toFile($thumb_newname);
							}
							else
							{
								$img->fromFile($_FILES['media']['tmp_name'][$name])->resize(450, null)->toFile($thumb_newname);
							}

							// if it's a gif, we need a static version to switch to a gif
							if ($ext == 'gif')
							{
								$static_pic = $uploaddir.$new_media_name.'_static.jpg';
								$img->fromFile($_FILES['media']['tmp_name'][$name])->overlay($_SERVER['DOCUMENT_ROOT'].'/templates/default/images/playbutton.png')->toFile($static_pic, 'image/jpeg');

								$static_url = $core->config('website_url') . 'uploads/articles/article_media/'.$new_media_name.'_static.jpg';
								$gif_static_button = '<button data-url-gif="'.$main_url.'" data-url-static="'.$static_url.'" class="add_static_button">Insert Static</button>';
							}
						}
						/* EXTERNAL FILE UPLOADS 
						This is for uploading to DO Spaces, AWS etc
						*/
						else
						{
							$thumb_url = $core->config('external_media_upload_url') . 'uploads/articles/article_media/thumbs/' . $image_name;

							// so we don't make a big thumbnail of a small image
							if ($img->getWidth() <= 450)
							{
								$thumb_file = $img->fromFile($_FILES['media']['tmp_name'][$name])->toString();
							}
							else
							{
								$thumb_file = $img->fromFile($_FILES['media']['tmp_name'][$name])->resize(450, null)->toString();
							}

							// if it's a gif, we need a static version to switch to a gif
							if ($ext == 'gif')
							{
								$static_pic = $new_media_name.'_static.jpg';
								$static_file = $img->fromFile($_FILES['media']['tmp_name'][$name])->overlay($_SERVER['DOCUMENT_ROOT'].'/templates/default/images/playbutton.png')->toString('image/jpeg');

								$static_url = $core->config('external_media_upload_url') . 'uploads/articles/article_media/'.$new_media_name.'_static.jpg';
								$gif_static_button = '<button data-url-gif="'.$main_url.'" data-url-static="'.$static_url.'" class="add_static_button">Insert Static</button>';

								$upload_details = [
									'Bucket' => 'goluploads',
									'Key'    => 'uploads/articles/article_media/' . $static_pic,
									'Body'   => $static_file,
									'ACL'    => 'public-read',
									'ContentType' => $mime
								];
								try {
									$result = $client->putObject($upload_details);
								} 
								catch (S3Exception $e) 
								{
									echo 'there has been an exception<br>';
									print_r($e);
								}
							}		

							$upload_details = [
								'Bucket' => 'goluploads',
								'Key'    => 'uploads/articles/article_media/thumbs/' . $image_name,
								'Body'   => $thumb_file,
								'ACL'    => 'public-read',
								'ContentType' => $mime
							];
							try {
								$result = $client->putObject($upload_details);
							} 
							catch (S3Exception $e) 
							{
								echo 'there has been an exception<br>';
								print_r($e);
							}
						}

						$thumbnail_button = '<button data-url="'.$thumb_url.'" data-main-url="'.$main_url.'" class="add_thumbnail_button">Insert thumbnail</button>';

						$preview_file = '<img src="' . $thumb_url . '" class="imgList"><br />';
						$data_type = 'image';
					}
					else if ($ext == 'mp4' || $ext == 'webm')
					{
						$preview_file = '<video width="100%" src="'.$main_url.'" controls></video>';
						$data_type = 'video';
					}
					else if ($ext == 'mp3' || $ext == 'ogg')
					{
						$preview_file = '<div class="ckeditor-html5-audio" style="text-align: center;"><audio controls="controls" src="'.$main_url.'">&nbsp;</audio></div>';
						$data_type = 'audio';
					}

					// upload the main file

					if (isset($_POST['local_upload']))
					{
						if (move_uploaded_file($_FILES['media']['tmp_name'][$name], $main_newname))
						{
							$finished = 1;
						}
					}
					else
					{
						$to_upload = fopen($_FILES['media']['tmp_name'][$name],'rb');

						$upload_details = [
							'Bucket' => 'goluploads',
							'Key'    => 'uploads/articles/article_media/' . $image_name,
							'Body'   => $to_upload,
							'ACL'    => 'public-read',
							'ContentType' => $mime
						];
						try {
							$result = $client->putObject($upload_details);
							$finished = 1;
						} 
						catch (S3Exception $e) 
						{
							echo 'there has been an exception<br>';
							print_r($e);
						}
					}
					
						
					if ($finished == 1)
					{
						$article_id = 0;
						if (isset($_POST['article_id']) && is_numeric($_POST['article_id']))
						{
							$article_id = $_POST['article_id'];
						}

						$new_image = $dbl->run("INSERT INTO `article_images` SET `filename` = ?, `location` = ?, `uploader_id` = ?, `date_uploaded` = ?, `article_id` = ?, `filetype` = ?", [$image_name, $location, $_SESSION['user_id'], core::$date, $article_id, $ext]);
						$media_db_id = $new_image->new_id();

						$html_output = '<div class="box">
						<div class="body group">
						<div id="'.$media_db_id.'">'.$preview_file.'
						URL: <input id="img' . $media_db_id . '" type="text" value="' . $main_url . '" /> <button class="btn" data-clipboard-target="#img' . $media_db_id . '">Copy</button> '.$gif_static_button.' <button data-url="'.$main_url.'" data-type="'.$data_type.'" class="add_button">Insert</button> '.$thumbnail_button.' <button id="' . $media_db_id . '" class="trash" data-type="article">Delete Media</button>
						</div>
						</div>
						</div>';

						$return_data[] = array('output' => $html_output, "media_id" => $media_db_id);
					}

					else
					{
						echo '<span class="imgList">You have exceeded the size limit! so moving unsuccessful! </span>';
					}

				}
				else
				{
					echo '<span class="imgList">You have exceeded the size limit!</span>';
				}

			}
			else
			{
				echo '<span class="imgList">Unknown extension or extension not allowed!</span>';
				return;
				die();
			}

		}
		echo json_encode(array("data" => $return_data));
		return;
	}
}
?>
