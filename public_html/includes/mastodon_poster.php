<?php
if(!defined('golapp'))
{
    die('Direct access not permitted');
}

defined('SOCIAL_HASHTAGS_ARRAY') or define('SOCIAL_HASHTAGS_ARRAY', array("#Linux", "#LinuxGaming", "#GamingNews", "#PCGaming"));

$hashtags = SOCIAL_HASHTAGS_ARRAY;

$headers = [
    'Authorization: Bearer ' . MASTODON_AUTH_KEY
  ];
  
  $status_data = array(
    "status" => BLUESKY_POST_TITLE . " " . BLUESKY_POST_LINK . "\r\n\r\n" . implode(' ', $hashtags),
    "language" => "eng",
    "visibility" => "public"
  );
  
  $ch_status = curl_init();
  curl_setopt($ch_status, CURLOPT_URL, "https://mastodon.social/api/v1/statuses");
  curl_setopt($ch_status, CURLOPT_POST, 1);
  curl_setopt($ch_status, CURLOPT_POSTFIELDS, $status_data);
  curl_setopt($ch_status, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch_status, CURLOPT_HTTPHEADER, $headers);
  
  $output_status = json_decode(curl_exec($ch_status));
  
  curl_close ($ch_status);
?>