<?php
use claviska\SimpleImage;

class image_upload
{
	protected $dbl;	
	public static $return_message;
	private $core;
	function __construct($dbl, $core)
	{
		$this->core = $core;
		$this->dbl = $dbl;
	}
	
	public function avatar($author_photo = 0)
	{
		if (is_uploaded_file($_FILES['new_image']['tmp_name']))
		{
			if ($_FILES['new_image']['size'] > $this->core->config('max_avatar_bytes')) // 100kb
			{
					self::$return_message = 'too_big';
					return false;
			}

			// this will make sure it is an image file, if it cant get an image size then its not an image
			if (!getimagesize($_FILES['new_image']['tmp_name']))
			{
				self::$return_message = 'not_image';
				return false;
			}

			// check the dimensions
			list($width, $height, $type, $attr) = getimagesize($_FILES['new_image']['tmp_name']);
			
			if ($width > $this->core->config('avatar_width') || $height > $this->core->config('avatar_height'))
			{
				$img = new SimpleImage();

				$img->fromFile($_FILES['new_image']['tmp_name'])->resize($this->core->config('avatar_width'), $this->core->config('avatar_height'))->toFile($_FILES['new_image']['tmp_name']);
			}

			// see if they currently have an avatar set
			$avatar = $this->dbl->run("SELECT `avatar`, `avatar_uploaded`, `author_picture` FROM `users` WHERE `user_id` = ?", array($_SESSION['user_id']))->fetch();

			$image_info = getimagesize($_FILES['new_image']['tmp_name']);
			$image_type = $image_info[2];
			$file_ext = '';
			if( $image_type == IMAGETYPE_JPEG )
			{
				$file_ext = 'jpg';
			}

			else if( $image_type == IMAGETYPE_GIF )
			{
				$file_ext = 'gif';
			}

			else if( $image_type == IMAGETYPE_PNG )
			{
				$file_ext = 'png';
			}

			$rand_name = rand(1,999);

			$imagename = $_SESSION['username'] . $rand_name . '_avatar.' . $file_ext;

			// the actual image
			$source = $_FILES['new_image']['tmp_name'];

			// where to upload to
			if ($author_photo == 0)
			{
				$target = $_SERVER['DOCUMENT_ROOT'] . "/uploads/avatars/" . $imagename;
			}
			else if ($author_photo == 1)
			{
				$target = $_SERVER['DOCUMENT_ROOT'] . "/uploads/avatars/author_pictures/" . $imagename;
			}

			if (move_uploaded_file($source, $target))
			{
				if ($author_photo == 0)
				{
					// remove old avatar
					if ($avatar['avatar_uploaded'] == 1)
					{
						unlink($_SERVER['DOCUMENT_ROOT'] . '/uploads/avatars/' . $avatar['avatar']);
					}

					$this->dbl->run("UPDATE `users` SET `avatar` = ?, `avatar_uploaded` = 1, `avatar_gallery` = NULL WHERE `user_id` = ?", array($imagename, $_SESSION['user_id']));
					return true;
				}
				else if ($author_photo == 1)
				{
					// remove old avatar
					if (isset($avatar['author_picture']) && !empty($avatar['author_picture']))
					{
						unlink($_SERVER['DOCUMENT_ROOT'] . '/uploads/avatars/author_pictures/' . $avatar['author_picture']);
					}

					$this->dbl->run("UPDATE `users` SET `author_picture` = ? WHERE `user_id` = ?", array($imagename, $_SESSION['user_id']));
					return true;
				}
			}

			else
			{
				self::$return_message = 'cant_upload';
				return false;
			}
		}

		else
		{
			self::$return_message = 'no_file';
			return false;
		}
	}
}
