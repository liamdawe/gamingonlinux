<?php
return [
	"url-characters" => 
	[
		"text"	=> 'Your profile URL had incorrect characters. We only allow letters, numbers, underscores and dashes.',
		"error" => 1
    ],
    "exists" =>
    [
        "text" => 'Sorry, that profile URL already exists. It needs to be unique.',
        "error" => 1
    ],
    "naughty" =>
    [
        "text" => 'Sorry, your profile url had terms in it we do not allow. Words like admin and moderator are banned.',
        "error" => 1
    ],
    "url-too-short" =>
    [
        "text" => 'Sorry, the profile URL was too short. Please make it at least 4 characters long.',
        "error" => 1
    ],
    "youtube-missing" =>
	[
		"text" => "That is not a correct Youtube URL format, please use a correct URL like: https://www.youtube.com/gamingonlinux",
		"error" => 1
	],
	"twitch-missing" =>
	[
		"text" => "That is not a correct Twitch URL format, please use a correct URL like: http://www.twitch.tv/gamingonlinux",
		"error" => 1
	],
	"broken_link" =>
	[
		"text" => "Your %s was missing either http or https - please ensure it's a correct link!",
		"error" => 1,
		"additions" => 1
	],
    "profile_updated" =>
	[
		"text" => "Profile updated!"
	],
	"bad_word" =>
	[
		"text" => "Your profile address contained words not allowed: %s",
		"error" => 1,
		"additions" => 1
	]
];
?>
