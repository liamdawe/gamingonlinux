<?php
return [
	"notfound" => 
	[
		"text" => "We couldn't find the people requested! Have you got the correct exact username? Please try again.",
		"error" => 1
	],
	"pm_sent" =>
	[
		"text" => "That DM has now been sent!"
	],
	"cannot_send_pm" =>
	[
		"text" => "Unable to send DM. They may have blocked you, you might have blocked them, or they may have requested to get no direct messages.",
		"error" => 1
	],
	"not_allowed" =>
	[
		"text" => "Sorry, but only moderators can send new Direct Messages.",
		"error" => 1
	]
];
