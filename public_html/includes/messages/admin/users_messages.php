<?php
return [
	"user_banned" =>
	[
		"text" => "That user has now been banned!",
	],
	"user_unbanned" => 
	[
		"text" => "That users ban has now been lifted!",
	],
	"user_content_deleted" =>
	[
		"text" => 'All the content from that user will now been deleted! Comments and forum posts may take a bit longer as it is run on a scheduled cron job.'
	],
	"ip_unban" =>
	[
		"text" => 'That IP ban has now been lifted!'
	],
	"ip_ban" =>
	[
		"text" => 'That IP has now been banned!'
	],
	"username_exists" =>
	[
		"text" => 'That username could not be changed, another user already has it!',
		"error" => 1
	],
	"cannot_remove_admin" =>
	[
		"text" => "You cannot remove the main admin account!",
		"error" => 1
	],
	"group_exists" =>
	[
		"text" => "Sorry but that group name exists already!",
		"error" => 1
	],
	"reason_needed" =>
	[
		"text" => "A reason is required when banning someone.",
		"error" => 1
	],
	"activated" =>
	[
		"text" => "User activated!"
	],
	"user_locked" =>
	[
		"text" => 'That user is currently locked for editing as it\'s currently open by %s. Lock is removed automatically after 10 minutes.',
		"additions" => 1
	]
];
?>
