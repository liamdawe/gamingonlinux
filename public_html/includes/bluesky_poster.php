<?php
if(!defined('golapp'))
{
    die('Direct access not permitted');
}

define( 'BLUESKY_API_HANDLE' , 'gamingonlinux.com' );
//define( 'BLUESKY_API_PASSWORD' , '' );
//define( 'BLUESKY_POST_LINK' , '' );
//define( 'BLUESKY_POST_TITLE' , '' );

$text=BLUESKY_POST_TITLE;
$link=BLUESKY_POST_LINK;

defined('SOCIAL_HASHTAGS_ARRAY') or define('SOCIAL_HASHTAGS_ARRAY', array("#Linux", "#LinuxGaming", "#GamingNews", "#PCGaming"));

$hashtags = SOCIAL_HASHTAGS_ARRAY;

$text = $text . "\r\n" . implode(' ', $hashtags);

function bluesky_connect() 
{
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://bsky.social/xrpc/com.atproto.server.createSession',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_POST => true,
        CURLOPT_POSTFIELDS =>'{
            "identifier":"'.BLUESKY_API_HANDLE.'",
            "password":"'.BLUESKY_API_PASSWORD.'"
    }',
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
        ),
    ));
    $response = curl_exec($curl);
    curl_close($curl);
    return json_decode($response,TRUE);
}

function upload_media_to_bluesky($session, $filename, $fileUploadDir = '/tmp') 
{
    // have we been passed a file?
    if (empty($filename)) return;
    // get the mime type, size and basename of the file
    $body = file_get_contents($filename);
    // If the path is a URL, use basename to get the filename
    if (filter_var($filename, FILTER_VALIDATE_URL)) {
        $basename = basename(parse_url($filename, PHP_URL_PATH));
    }
    else 
    {
        // If the path is a local path, use basename to get the filename
        $basename = basename($filename);
    }
    $size = strlen($body);
    // does the file size need reducing?
    if ($size > 1000000) {
        $newImage = imagecreatefromstring($body);
        // downsample the image until it is less than maxImageSize (if possible!)
        for ($i = 9; $i >= 1; $i--) 
        {
            imagejpeg($newImage, $fileUploadDir.'/'.$basename,$i * 10);
            $size = strlen(file_get_contents($fileUploadDir.'/'.$basename));
            if ($size < 1000000) {
                break;
            }
            else 
            {
                unlink($fileUploadDir.'/'.$basename);
            }
        }
        $body = file_get_contents($fileUploadDir.'/'.$basename);
        unlink($fileUploadDir.'/'.$basename);
    }
    // get the file mime type
    if (filter_var($filename, FILTER_VALIDATE_URL)) 
    {
        $headers = get_headers($filename, 1);
        if (isset($headers['Content-Type'])) 
        {
            $content_type = $headers['Content-Type'];
        }
    }
    else 
    {
        $content_type = mime_content_type($filename);
    }
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://bsky.social/xrpc/com.atproto.repo.uploadBlob',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_POST => true,
        CURLOPT_POSTFIELDS => $body,
        CURLOPT_HTTPHEADER => array(
            'Content-Type: '.$content_type,
            'Authorization: Bearer '.$session['accessJwt']
        ),
    ));
    $response = curl_exec($curl);
    curl_close($curl);
    $response = json_decode($response, false, 512, JSON_THROW_ON_ERROR);
    $image = $response->blob;
    return $image;
}

function fetch_link_card($session, $url) 
{
    // The required fields for every embed card
    $card = [
        "uri" => $url,
        "title" => "",
        "description" => "",
    ];
    // Create a new DOMDocument
    $doc = new DOMDocument();
    // Suppress errors for invalid HTML, if needed
    libxml_use_internal_errors(true);
    // Load the HTML from the URL
    $doc->loadHTMLFile($url);
    // Restore error handling
    libxml_use_internal_errors(false);
    // Create a new DOMXPath object for querying the document
    $xpath = new DOMXPath($doc);
    // Query for "og:title" and "og:description" meta tags
    $title_tag = $xpath->query('//meta[@property="og:title"]/@content');
    if ($title_tag->length > 0) {
        $card["title"] = $title_tag[0]->nodeValue;
    }
    $description_tag = $xpath->query('//meta[@property="og:description"]/@content');
    if ($description_tag->length > 0) {
        $card["description"] = $description_tag[0]->nodeValue;
    }
    // If there is an "og:image" meta tag, fetch and upload that image
    $image_tag = $xpath->query('//meta[@property="og:image"]/@content');
    if ($image_tag->length > 0) {
        $img_url = $image_tag[0]->nodeValue;
        // Naively turn a "relative" URL (just a path) into a full URL, if needed
        if (!parse_url($img_url, PHP_URL_SCHEME)) {
            $img_url = $url . $img_url;
        }
        $image = upload_media_to_bluesky($session, $img_url);
    }
    $embed = [
        '$type' => 'app.bsky.embed.external',
        'external' => [
            'uri' => $card['uri'],
            'title' => $card['title'],
            'description' => $card['description'],
            'thumb' => $image,
        ],
    ];
    return $embed;
}

function hashtags($text)
{
    // Detect hashtags using preg_match_all
    preg_match_all("/#[a-zA-Z0-9&\-\_]+/", $text, $matches, PREG_OFFSET_CAPTURE);

    foreach ($matches[0] as $match)
    {
        $hashtag = $match[0];
        $byteStart = $match[1];
        $byteEnd = $byteStart + strlen($hashtag);

        // Create facet for each hashtag
        $result[] = [
            'index' => [
                'byteStart' => $byteStart,
                'byteEnd' => $byteEnd
            ],
            'features' => [
                [
                    '$type' => 'app.bsky.richtext.facet#tag',
                    'tag' => substr($hashtag, 1) // Remove the # from the beginning
                ]
            ]
        ];
    }

    return $result;
}

function bluesky_post($session, $text , $link) 
{
    $args = [
        'repo' => $session['did'],
        'collection' => 'app.bsky.feed.post',
        'record' => [
            '$type' => 'app.bsky.feed.post',
            'createdAt' => date('c'),
            'text' => $text,
            'langs' => ['en'],
            'embed' => fetch_link_card($session, $link),
            'facets' => []
        ],
    ];

    $args['record']['facets'] = hashtags($text);

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://bsky.social/xrpc/com.atproto.repo.createRecord',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_POST => true,
        CURLOPT_POSTFIELDS => json_encode( $args ),
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'Authorization: Bearer '.$session['accessJwt']
        ),
    ));
    $response = curl_exec($curl);
    curl_close($curl);
}

$session = bluesky_connect();

bluesky_post($session, $text, $link);
?>
