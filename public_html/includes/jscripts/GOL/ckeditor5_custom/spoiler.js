import { Plugin, ButtonView } from 'ckeditor5';

export default class Spoiler extends Plugin {
	init() {
		const editor = this.editor;

		editor.ui.componentFactory.add( 'Spoiler', () => {
			
			const button = new ButtonView();

			button.set( {
				label: 'Spoiler',
				tooltip: 'Spoiler',
				icon: '<svg fill="#000000" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="800px" height="800px" viewBox="0 0 45.311 45.311" xml:space="preserve"><g><path d="M22.675,0.02c-0.006,0-0.014,0.001-0.02,0.001c-0.007,0-0.013-0.001-0.02-0.001C10.135,0.02,0,10.154,0,22.656c0,12.5,10.135,22.635,22.635,22.635c0.007,0,0.013,0,0.02,0c0.006,0,0.014,0,0.02,0c12.5,0,22.635-10.135,22.635-22.635C45.311,10.154,35.176,0.02,22.675,0.02z M22.675,38.811c-0.006,0-0.014-0.001-0.02-0.001c-0.007,0-0.013,0.001-0.02,0.001c-2.046,0-3.705-1.658-3.705-3.705c0-2.045,1.659-3.703,3.705-3.703c0.007,0,0.013,0,0.02,0c0.006,0,0.014,0,0.02,0c2.045,0,3.706,1.658,3.706,3.703C26.381,37.152,24.723,38.811,22.675,38.811z M27.988,10.578c-0.242,3.697-1.932,14.692-1.932,14.692c0,1.854-1.519,3.356-3.373,3.356c-0.01,0-0.02,0-0.029,0c-0.009,0-0.02,0-0.029,0c-1.853,0-3.372-1.504-3.372-3.356c0,0-1.689-10.995-1.931-14.692C17.202,8.727,18.62,5.29,22.626,5.29c0.01,0,0.02,0.001,0.029,0.001c0.009,0,0.019-0.001,0.029-0.001C26.689,5.29,28.109,8.727,27.988,10.578z"/></g></svg>',
				withText: false
			} );

			// Execute a callback function when the button is clicked
			button.on( 'execute', () => {

				// Change the model using the model writer
				editor.model.change( writer => {
                    const content =
                    '<details class="spoiler_container"><summary class="spoiler_header">Spoiler, click me</summary><div class="spoiler-content">Text here</div></details>';
                    const viewFragment = editor.data.processor.toView( content );
                    const modelFragment = editor.data.toModel( viewFragment );
                
                    editor.model.insertContent( modelFragment );
				} );
			} );

			return button;
		} );
	}
}