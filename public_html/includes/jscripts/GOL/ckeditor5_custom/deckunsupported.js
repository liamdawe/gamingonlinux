import { Plugin, ButtonView } from 'ckeditor5';

export default class DeckUnsupported extends Plugin {
	init() {
		const editor = this.editor;

		editor.ui.componentFactory.add( 'DeckUnsupported', () => {
			
			const button = new ButtonView();

			button.set( {
				label: 'Deck Unsupported',
				tooltip: 'Deck Unsupported',
				icon: '<svg viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M14.1931 15.6064C13.0246 16.4816 11.5733 17 10.001 17C6.13498 17 3.00098 13.866 3.00098 10C3.00098 8.42766 3.51938 6.97641 4.39459 5.80783L14.1931 15.6064ZM15.6074 14.1922C16.4826 13.0236 17.001 11.5723 17.001 10C17.001 6.13401 13.867 3 10.001 3C8.42864 3 6.97739 3.5184 5.80881 4.39362L15.6074 14.1922ZM19.001 10C19.001 14.9706 14.9715 19 10.001 19C5.03041 19 1.00098 14.9706 1.00098 10C1.00098 5.02944 5.03041 1 10.001 1C14.9715 1 19.001 5.02944 19.001 10Z" fill="#D22B2B"></path></svg>',
				withText: false
			} );

			// Execute a callback function when the button is clicked
			button.on( 'execute', () => {

				// Change the model using the model writer
				editor.model.change( writer => {
					const imageElement = writer.createElement('imageInline', {
						src: '/templates/default/images/steamdeck/deck_unsupported.png',
						width: '20px',
						alt: 'Deck Not Unsupported'
					});
					editor.model.insertContent(imageElement, editor.model.document.selection);
				} );
			} );

			return button;
		} );
	}
}