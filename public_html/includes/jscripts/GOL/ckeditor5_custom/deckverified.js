import { Plugin, ButtonView } from 'ckeditor5';

export default class DeckVerified extends Plugin {
	init() {
		const editor = this.editor;

		editor.ui.componentFactory.add( 'DeckVerified', () => {
			
			const button = new ButtonView();

			button.set( {
				label: 'Deck Verified',
				tooltip: 'Deck Verified',
				icon: '<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="1 1 18 18"><path fill-rule="evenodd" clip-rule="evenodd" d="M10 19C14.9706 19 19 14.9706 19 10C19 5.02944 14.9706 1 10 1C5.02944 1 1 5.02944 1 10C1 14.9706 5.02944 19 10 19ZM8.33342 11.9222L14.4945 5.76667L16.4556 7.72779L8.33342 15.8556L3.26675 10.7833L5.22786 8.82223L8.33342 11.9222Z" fill="#59BF40"/></svg>',
				withText: false
			} );

			// Execute a callback function when the button is clicked
			button.on( 'execute', () => {

				// Change the model using the model writer
				editor.model.change( writer => {
					const imageElement = writer.createElement('imageInline', {
						src: '/templates/default/images/steamdeck/deck_verified.png',
						width: '20px',
						alt: 'Deck Verified'
					});
					editor.model.insertContent(imageElement, editor.model.document.selection);
				} );
			} );

			return button;
		} );
	}
}