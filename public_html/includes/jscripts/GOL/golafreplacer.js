function gol_affiliates()
{
    // get all humble store links, add our partner ID
	const humble_links = document.querySelectorAll('div.article a[href*="//www.humblebundle.com"], div.database-extra a[href*="//www.humblebundle.com"]');
	
	$( humble_links ).each(function( index ) 
	{
		var _href = $(this).attr("href"); 
        var stripped_url = _href.split("?")[0];
		$(this).attr('href', 'https://humblebundleinc.sjv.io/c/5772989/2059850/25796?u=' + stripped_url);
	});

	// get all GOG store links, add our partner ID
	const gog_links = document.querySelectorAll('div.article a[href*="//www.gog.com"], div.article a[href*="//gog.com"], div.database-extra a[href*="//www.gog.com"], div.database-extra a[href*="//gog.com"]');

	$( gog_links ).each(function( index ) 
	{
		var _href = $(this).attr("href"); 

		_href = _href.replace("?pp=b2a10a6c3dcadb10c8ffd734c1bab896d55cf0ec", ""); // remove their old ID as it causes issues

		var url_split = new URL(_href);

		if (url_split.pathname !== null)
		{
			if (url_split.pathname.includes('forum') || url_split.pathname.includes('account'))
			{
				// leave these alone, they break the redirect and don't work
			}
			else if (url_split.search === null || url_split.search === '')
			{
				$(this).attr('href', 'https://af.gog.com' + url_split.pathname + '?as=1636858786');
			}
			else
			{
				$(this).attr('href', 'https://af.gog.com' + url_split.pathname + url_split.search + '&as=1636858786');
			}
		}
		else
		{
			$(this).attr('href', 'https://af.gog.com/?as=1636858786');
		}
	});	

	// change all fanatical links
	const fanatical_links = document.querySelectorAll('div.article a[href*="//www.fanatical.com"], div.article a[href*="//fanatical.com"], div.database-extra a[href*="//www.fanatical.com"], div.database-extra a[href*="//fanatical.com"]');

	$( fanatical_links ).each(function( index )
	{
		var _href = $(this).attr("href"); 

		var fanatical_split = new URL(_href);

		if (fanatical_split.pathname !== null)
		{
			if (fanatical_split.search === null || fanatical_split.search === '')
			{
				$(this).attr('href', 'https://www.fanatical.com' + fanatical_split.pathname + '?ref=gol');
			}
			else
			{
				$(this).attr('href', 'https://www.fanatical.com' + fanatical_split.pathname + fanatical_split.search + '&ref=gol');
			}
		}
		else
		{
			$(this).attr('href', 'https://www.fanatical.com/?ref=gol');
		}
	});
}