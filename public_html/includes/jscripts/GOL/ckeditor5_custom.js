import { Plugin, ButtonView } from 'ckeditor5';

import DeckVerified from './ckeditor5_custom/deckverified.js';
import DeckPlayable from './ckeditor5_custom/deckplayable.js';
import DeckUnsupported from './ckeditor5_custom/deckunsupported.js';
import DeckNotRated from './ckeditor5_custom/decknotrated.js';
import Spoiler from './ckeditor5_custom/spoiler.js';

export default class DeckRatings extends Plugin {
    static get requires() {
        return [ DeckVerified, DeckPlayable, DeckUnsupported, DeckNotRated, Spoiler ];
    }
}