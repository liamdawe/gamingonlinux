<?php
if(!defined('golapp')) 
{
	die('Direct access not permitted');
}

require_once("includes/curl_data.php");

$templating->set_previous('meta_description', 'Login page for GamingOnLinux', 1);

// check for failed login attempts
$captcha_needed = 0;
$check_ips = $dbl->run("SELECT COUNT(`ip_address`) FROM `failed_logins` WHERE `ip_address` = ? AND `attempted` >= NOW() - INTERVAL 10 MINUTE", array(core::$ip))->fetchOne();
if ($check_ips >= 10)
{
	$captcha_needed = 1;
}

$templating->load('login');

if (!isset($_POST['action']))
{
	if (!isset($_GET['forgot']) && !isset($_GET['reset']))
	{
		$templating->set_previous('title', 'Login', 1);
		$templating->set_previous('canonical_link', '<link rel="canonical" href="'.$core->config('website_url') . 'index.php?module=login">', 1);

		if (isset($_SESSION['user_id']) && $_SESSION['user_id'] == 0 || !isset($_SESSION['user_id']))
		{
			$user_session->login_form(NULL, $captcha_needed);
		}

		else
		{
			$core->message("You are already logged in!", 1);
		}
	}

	else if (isset($_GET['forgot']))
	{
		$templating->set_previous('title', 'Forgotten Login', 1);
		$templating->set_previous('canonical_link', '<link rel="canonical" href="'.$core->config('website_url') . 'index.php?module=login&forgot">', 1);

		if ($core->config('captcha_disabled') == 0)
		{
			$captcha = '<strong>You must do a captcha to register</strong><br />
			We use Google\'s reCAPTCHA, you must agree to their use of cookies to use it. This is to help us prevent spam!
			<button id="accept_captcha" type="button" data-pub-key="'.$core->config('recaptcha_public').'">Accept & Show reCAPTCHA</button>';
		}
		else
		{
			$captcha = '';
		}

		$templating->block('forgot', 'login');
		$templating->set('captcha', $captcha);
	}

	else if (isset($_GET['reset']))
	{
		$templating->set_previous('title', 'Reset Login', 1);
		$templating->set_previous('canonical_link', '<link rel="canonical" href="'.$core->config('website_url') . 'index.php?module=login&reset">', 1);

		$email = $_GET['email'];
		$code = $_GET['code'];

		// check its a valid time
		$get_time = $dbl->run("SELECT `user_email`, `expires` FROM `password_reset` WHERE `user_email` = ? AND `secret_code` = ?", array($email, $code))->fetch();

		// check code and email is valid
		if (!$get_time)
		{
			$core->message("That is not a correct password reset request, you will need to <a href=\"/index.php?module=login&forgot\">request a new code!</a>");
		}

		else if (time() > $get_time['expires'])
		{
			// drop any previous requested
			$dbl->run("DELETE FROM `password_reset` WHERE `user_email` = ?", array($email));

			$core->message("That reset request has expired, you will need to <a href=\"/index.php?module=login&forgot\">request a new code!</a>");
		}

		else
		{
			$url_email = rawurlencode($_GET['email']);
			$templating->block('reset');
			$templating->set('code', $code);
			$templating->set('email', $url_email);
		}
	}
}

else if (isset($_POST['action']))
{
	if ($_POST['action'] == 'Login')
	{
		if ($captcha_needed == 1)
		{
			if (!isset($_POST['g-recaptcha-response']))
			{
				$_SESSION['message'] = 'captcha_nope';
				header("Location: /index.php?module=login");
				die();
			}
			$recaptcha = $_POST['g-recaptcha-response'];
			$google_url = "https://www.google.com/recaptcha/api/siteverify";
			$ip = core::$ip;
			$url = $google_url."?secret=".$core->config('recaptcha_secret')."&response=".$recaptcha."&remoteip=".$ip;
			$res = getCurlData($url);
			$res = json_decode($res, true);

			if (!$res['success'])
			{
				$_SESSION['message'] = 'captcha_nope';
				header("Location: /index.php?module=login");
				die();
			}
		}

		$stay = 0;
		if (isset($_POST['stay']))
		{
			$stay = 1;
		}

		if ($user->login($_POST['username'], $_POST['password'], $stay) == true)
		{
			unset($_SESSION['login_error']);
			unset($_SESSION['login_error_username']);

			// if the login form had a current page set, we need to check to see if we can redirect
			if(!empty($_POST['current_page'])) 
			{
				$parse_url = parse_url($_POST['current_page']);

				if (!empty($parse_url['scheme']) && $parse_url['scheme'].'://'.$parse_url['host'].'/' == $core->config('website_url'))
				{
					$extra = '';
					if (isset($parse_url['query']) && !empty($parse_url['query']))
					{
						$extra .= '?'.$parse_url['query'];
					}

					$path = substr($parse_url['path'], 1); // remove the slash at the start so we don't double up
					header("Location: ".$core->config('website_url').$path.$extra);
					die();
				}
				else
				{
					header("Location: ".$core->config('website_url'));
					die();
				}
			}
			else
			{
				header("Location: ".$core->config('website_url'));
				die();
			}
		}

		else
		{
			$dbl->run("INSERT INTO `failed_logins` SET `id` = UUID(), `username` = ?, `ip_address` = ?, `attempted` = CURRENT_TIMESTAMP", array($_POST['username'], core::$ip));
			$_SESSION['login_error_username'] = htmlspecialchars($_POST['username']);
			header("Location: ".$core->config('website_url')."index.php?module=login");
			die();
		}
	}

	else if ($_POST['action'] == 'Send')
	{
		// check captcha to prevent botting
		if ($core->config('captcha_disabled') == 0)
		{
			if (!isset($_POST['g-recaptcha-response']))
			{
				$_SESSION['message'] = 'captcha_nope';
				header("Location: /index.php?module=login&forgot");
				die();
			}
			$recaptcha = $_POST['g-recaptcha-response'];
			$google_url = "https://www.google.com/recaptcha/api/siteverify";
			$ip = core::$ip;
			$url = $google_url."?secret=".$core->config('recaptcha_secret')."&response=".$recaptcha."&remoteip=".$ip;
			$res = getCurlData($url);
			$res = json_decode($res, true);
		}

		if ($core->config('captcha_disabled') == 1 || ($res['success']))
		{
			// reset rate limit by session, real basic security slow-down measure
			if (!isset($_SESSION['pass_reset_counter']))
			{
				$_SESSION['pass_reset_counter'] = 1;
			}
			else if ($_SESSION['pass_reset_counter'] >= 3)
			{
				$_SESSION['message'] = 'limits_hit';
				header("Location: ".$core->config('website_url')."index.php?module=login&forgot");
				die();			
			}
			else
			{
				$_SESSION['pass_reset_counter']++;
			}

			// check if user exists
			$check_res = $dbl->run("SELECT `email` FROM `users` WHERE `email` = ?", array($_POST['email']))->fetch();
			if (!$check_res)
			{
				$_SESSION['message'] = 'completed_reset';
				header("Location: ".$core->config('website_url')."index.php?module=login&forgot");
				die();
			}

			else if ($check_res)
			{
				// first check rate limiting
				$count_times = $dbl->run("SELECT COUNT(*) FROM `password_reset` WHERE `user_email` = ?", array($_POST['email']))->fetchOne();

				if ($count_times >= 3)
				{
					$_SESSION['message'] = 'reset_locked';
					header("Location: ".$core->config('website_url')."index.php?module=login&forgot");				
					die();
				}
				$random_string = random_int(0,999999);

				// scramble any previous requested to prevent breakins from older stuff, as they're now just used to check rate limit and this new one might be the real deal
				$dbl->run("UPDATE `password_reset` SET `secret_code` = UUID() WHERE `user_email` = ?", array($_POST['email']));

				// make expiry 7 days from now
				$next_week = time() + (7 * 24 * 60 * 60);

				$manual_code = random_int(0,999999);

				// insert number to database with email
				$dbl->run("INSERT INTO `password_reset` SET `reset_id` = UUID(), `user_email` = ?, `secret_code` = ?, `manual_code` = ?, `expires` = ?", array($_POST['email'], $random_string, $manual_code, $next_week));

				$url_email = rawurlencode($_POST['email']);

				// send mail with link including the key
				$html_message = '<p>Someone, hopefully you, has requested to reset your password on ' . $core->config('website_url') . '!</p>
				<p>If you didn\'t request this, don\'t worry! Unless someone has access to your email address it isn\'t an issue!</p>
				<p>Please click <a href="' . $core->config('website_url') . 'index.php?module=login&reset&code=' . $random_string . '&email=' . $url_email . '" rel="noopener noreferrer">this link</a> to reset your password. You will also need to manually enter this Reset Code for extra security: <strong>'.$manual_code.'</strong></p>';

				$plain_message = 'Someone, hopefully you, has requested to reset your password on ' . $core->config('website_url') . '! Please go here: "' . $core->config('website_url') . 'index.php?module=login&reset&code=' . $random_string . '&email=' . $url_email . '" to change your password. If you didn\'t request this, you can ignore it as it\'s not a problem unless anyone has access to your email! You will also need to manually enter this Reset Code code for extra security: ' . $manual_code;

				// Mail it
				if ($core->config('send_emails') == 1)
				{
					$mail = new mailer($core);
					$mail->sendMail($_POST['email'], 'GamingOnLinux password reset request', $html_message, $plain_message);

					$_SESSION['message'] = 'completed_reset';
					header("Location: ".$core->config('website_url')."index.php?module=login&forgot");
					die();
				}
			}
		}
		else if ($core->config('captcha_disabled') == 0 && !$res['success'])
		{
			$_SESSION['message'] = 'captcha_nope';
			header("Location: /index.php?module=login&forgot");
			die();			
		}
	}

	// actually change the password as their code was correct and password + confirmation matched
	else if ($_POST['action'] == 'Reset')
	{
		$email = $_GET['email'];
		$code = $_GET['code'];
		$manual_code = $_POST['manual_code'];

		// check its a valid time
		$get_time = $dbl->run("SELECT `user_email`, `expires` FROM `password_reset` WHERE `user_email` = ? AND `secret_code` = ? AND `manual_code` = ?", array($email, $code, $manual_code))->fetch();
		
		// check code and email is valid
		if (!$get_time)
		{
			$core->message("That is not a correct password reset request, you will need to <a href=\"/index.php?module=login&forgot\">request a new code!</a>");
		}
		
		else if (time() > $get_time['expires'])
		{
			// drop any previous requested
			$dbl->run("DELETE FROM `password_reset` WHERE `user_email` = ?", array($email));
		
			$core->message("That reset request has expired, you will need to <a href=\"/index.php?module=login&forgot\">request a new code!</a>");
		}

		else
		{
			// check the passwords match
			if ($_POST['password'] != $_POST['password_again'])
			{
				$core->message("The new passwords didn't match! <a href=\"".$core->config('website_url')."index.php?module=login\">Go back.</a>");
			}

			// change the password
			else
			{
				$new_password = password_hash($_POST['password'], PASSWORD_BCRYPT);

				// new password
				$dbl->run("UPDATE `users` SET `password` = ? WHERE `email` = ?", array($new_password, $email));

				// drop any previous requested
				$dbl->run("DELETE FROM `password_reset` WHERE `user_email` = ?", array($email));

				$user_reset_id = $dbl->run("SELECT `user_id` FROM `users` WHERE `email` = ?", array($email))->fetchOne();

				$dbl->run("DELETE FROM `saved_sessions` WHERE `user_id` = ?", array($user_reset_id));

				$user->logout();

				header("Location " . url . '/index.php?module=login');
				die();
			}
		}
	}
}
?>
