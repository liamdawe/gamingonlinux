<?php
if(!defined('golapp')) 
{
	die('Direct access not permitted');
}
$templating->set_previous('meta_description', $core->config('meta_description'), 1);

$structured_info = "<script type=\"application/ld+json\">
{
	\"@context\": \"https://schema.org\",
	\"@type\": \"Organization\",
	\"name\": \"GamingOnLinux\",
	\"url\": \"https://www.gamingonlinux.com\",
	\"logo\": {
        \"@type\": \"ImageObject\",
        \"url\": \"https://www.gamingonlinux.com/templates/default/images/logos/icon_large.png\",
        \"width\": 349,
        \"height\": 420
    },
	\"sameAs\": [
        \"https://www.facebook.com/gamingonlinux\",
        \"https://twitter.com/GOLTwit\"
    ],
    \"keywords\": [
        \"Linux Gaming\",
        \"Gaming On Linux\",
        \"SteamOS\",
		\"Steam Deck\",
		\"Linux Games\",
		\"linux gaming news\"
    ]
}</script>";

$twitter_card = '<meta name="twitter:card" content="summary_large_image">'.PHP_EOL;
$twitter_card .= '<meta name="twitter:site" content="@'.$core->config('twitter_username').'">'.PHP_EOL;
$twitter_card .= '<meta name="twitter:title" content="GamingOnLinux">'.PHP_EOL;
$twitter_card .= '<meta name="twitter:description" content="'.$core->config('meta_description').'">'.PHP_EOL;
$twitter_card .= '<meta name="twitter:image" content="'.url.'templates/default/images/logos/twitter_card_icon.png">'.PHP_EOL;
$twitter_card .= '<meta name="twitter:image:src" content="'.url.'templates/default/images/logos/twitter_card_icon.png">'.PHP_EOL;

$templating->set_previous('meta_data', $structured_info . $twitter_card, 1);

$templating->load('home');

if (!isset($_GET['view']))
{
	// paging for pagination
	$page = core::give_page();

	if (!isset($page) || $page == 0 || $page == 1)
	{
		$templating->set_previous('title', $core->config('meta_homepage_title'), 1);
		$page_text_top = '';
	}
	else if ($page >= 2)
	{
		$templating->set_previous('title', $core->config('meta_homepage_title') . ' - page ' . $page, 1);
		$page_text_top = ' - Page: ' . $page;
	}

	if (!isset($_SESSION['hide_adverts']) || (isset($_SESSION['hide_adverts']) && $_SESSION['hide_adverts'] == 0))
	{
		$templating->block('adverts', 'home');
	}

	$templating->block('articles_top', 'home');
	$templating->set('page', $page_text_top);
	$templating->set('website_url', url);
	
	$quick_nav = '';
	if ($core->config('quick_nav') == 1)
	{
		$quick_nav = $templating->block_store('quick_nav', 'home');
		
		$quick_tag_hidden = [];
		$quick_tag_normal = [];
		$qn_res = $dbl->run("SELECT `category_name` FROM `articles_categorys` WHERE `quick_nav` = 1")->fetch_all(PDO::FETCH_BOTH);
		foreach ($qn_res as $get_quick)
		{
			$quick_tag_hidden[] = '<li><a href="'.$article_class->tag_link($get_quick['category_name']).'">'.$get_quick['category_name'].'</a></li>';
			$quick_tag_normal[] = ' <a class="link_button" href="'.$article_class->tag_link($get_quick['category_name']).'">'.$get_quick['category_name'].'</a> ';
		}
		
		$quick_nav = $templating->store_replace($quick_nav, ['quick_tag_hidden' => implode($quick_tag_hidden), 'quick_tag_normal' => implode($quick_tag_normal)]);
	}
	
	$templating->set('quick_nav', $quick_nav);

	// count how many there is in total
	if (($total = $core->get_dbcache('total_articles_active')) === false) // there's no cache
	{
		$total = $dbl->run("SELECT COUNT(`article_id`) FROM `articles` WHERE `active` = 1")->fetchOne();
		$core->set_dbcache('total_articles_active', $total);
	}

	if ($total)
	{
		if (!isset(core::$url_command[0]) || (isset(core::$url_command[0]) && core::$url_command[0] == 'home'))
		{
			$templating->set_previous('canonical_link', '<link rel="canonical" href="'.$core->config('website_url').'">', 1);

			$in  = str_repeat('?,', count($user->blocked_tags) - 1) . '?';
			$pagination_target = '/home/';
			if (!empty($user->blocked_tags) && $user->blocked_tags[0] != 0) // can't rely on counter cache if they're blocking tags
			{
				$total_blocked = $dbl->run("SELECT count(*) FROM article_category_reference c LEFT JOIN `articles` a ON a.article_id = c.article_id WHERE c.`category_id` IN ( $in ) AND a.active = 1", $user->blocked_tags)->fetchOne();
				$total = $total - $total_blocked;
			}
		}
		else if (isset(core::$url_command[0]) && core::$url_command[0] == 'all-articles')
		{
			if (!isset($page) || $page == 0 || $page == 1)
			{
				$templating->set_previous('title', 'All the latest Linux gaming news', 1);
				$templating->set_previous('canonical_link', '<link rel="canonical" href="'.$core->config('website_url') . 'all-articles/'.'">', 1);
			}
			else if ($page >= 2)
			{
				$templating->set_previous('title', 'All the latest Linux gaming news - page ' . $page, 1);
				$templating->set_previous('canonical_link', '<link rel="canonical" href="'.$core->config('website_url') . 'all-articles/'.'page='.$page.'/">', 1);
			}

			$in = '?';
			$user->blocked_tags = [0 => 0];
			$pagination_target = '/all-articles/';

			$templating->block('view_all');
		}

		$per_page = 15;
		if (isset($_SESSION['articles-per-page']) && is_numeric($_SESSION['articles-per-page']))
		{
			$per_page = $_SESSION['articles-per-page'];
		}
		
		$last_page = ceil($total/$per_page);
		
		if ($page > $last_page)
		{
			$page = $last_page;
		}

		// sort out the pagination link
		$pagination = $core->pagination_link($per_page, $total, $pagination_target, $page);

		// latest news
		$query = "SELECT a.`article_id`, a.`author_id`, a.`guest_username`, a.`title`, a.`tagline`, a.`text`, a.`date`, a.`comment_count`, a.`tagline_image`, a.`show_in_menu`, a.`slug`, a.`gallery_tagline`, a.`thumbnail_alt_text`, t.`filename` as gallery_tagline_filename, u.`username`, u.`profile_address`, a.`show_updated` FROM `articles` a LEFT JOIN `users` u on a.`author_id` = u.`user_id` LEFT JOIN `articles_tagline_gallery` t ON t.`id` = a.`gallery_tagline` WHERE a.`active` = 1 AND NOT EXISTS (SELECT 1 FROM article_category_reference c  WHERE a.article_id = c.article_id AND c.`category_id` IN ( $in )) ORDER BY a.`date` DESC LIMIT ?, ?";

		$articles_get = $dbl->run($query, array_merge($user->blocked_tags, [$core->start], [$per_page]))->fetch_all(PDO::FETCH_BOTH);

		if ($articles_get)
		{
			$article_id_array = array();

			foreach ($articles_get as $article)
			{
				$article_id_array[] = $article['article_id'];
			}
			$article_id_sql = implode(', ', $article_id_array);

			$get_categories = $article_class->find_article_tags(array('article_ids' => $article_id_sql, 'limit' => 5));
			
			$templating->load('articles');

			$article_class->display_article_list($articles_get, $get_categories);

			$templating->block('bottom', 'home');
			$templating->set('pagination', $pagination);
		}
	}
}

if (isset($_GET['view']) && $_GET['view'] == 'removeeditors')
{
	if (isset($_GET['article_id']) && is_numeric($_GET['article_id']))
	{
		$article_class->remove_editor_pick($_GET['article_id']);

		header("Location: ".$core->config('website_url')."home/");
		die();
	}
}
