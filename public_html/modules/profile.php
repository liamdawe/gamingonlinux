<?php
if(!defined('golapp')) 
{
	die('Direct access not permitted');
}

// check user exists
$profile_id = NULL;
if (isset($_GET['user_id']) && is_numeric($_GET['user_id']))
{
    $profile_id = (int) $_GET['user_id'];
}
else if (isset(core::$url_command[0]) && isset(core::$url_command[1]))
{
    $find_user = $dbl->run("SELECT `user_id` FROM `users` WHERE `profile_address` = ? OR `user_id` = ?", array(core::$url_command[1], core::$url_command[1]))->fetch();
    if ($find_user)
    {
        $profile_id = $find_user['user_id'];
    }
}
else
{
	http_response_code(404);
	$templating->set_previous('title', 'Linux Gamer User Profile - not found', 1);
	$core->message('No user id asked for to view! <a href="'.url.'">Click here to return home</a>.');
}
	
if ($profile_id == 1844)
{
	$templating->set_previous('title', 'Linux Gamer User Profile - bot', 1);
	$core->message('This is a bot.');
}
else
{
    $templating->load('profile');

    if (!isset($_GET['view']) && !isset(core::$url_command[2]))
    {
		$templating->set_previous('canonical_link', '<link rel="canonical" href="'.$core->config('website_url') . 'profiles/'.$profile_id.'">', 1);

        include('includes/profile_fields.php');

        $db_grab_fields = '';
        foreach ($profile_fields as $field)
        {
            $db_grab_fields .= "{$field['db_field']},";
        }

        $profile = $dbl->run("SELECT `user_id`, `pc_info_public`, `username`, `distro`, `register_date`, `email`, `avatar`, `avatar_uploaded`, `avatar_gallery`, `comment_count`, `forum_posts`, $db_grab_fields `article_bio`, `last_login`, `banned`, `ip`, `game_developer`, `private_profile`, `get_pms`, `show_supporter_status` FROM `users` WHERE `user_id` = ?", array($profile_id))->fetch();
        if (!$profile)
        {
			$templating->set_previous('title', 'Linux Gamer User Profile - doesn\'t exist', 1);
            $core->message('That person does not exist here!');
        }
        else
        {
			$templating->set_previous('title', 'Linux Gamer User Profile - ' . $profile['username'], 1);

            if (isset($_GET['go']) && $_GET['go'] == 'report')
            {
                $templating->block('report', 'profile');
                $templating->set('back_url', '/profiles/'.$profile['user_id']);
                $templating->set('action_url', '/index.php?module=profile&user_id='.$profile['user_id']);

                $illegal_text = '';
                if (isset($_SESSION['form_error']) && !empty($_SESSION['form_error']['illegal_reason']))
                {
                    $illegal_text = $_SESSION['form_error']['illegal_reason'];
                }
                $templating->set('illegal_text', $illegal_text);

                unset($_SESSION['form_error']);
            }

			$templating->block('top', 'profile');

			$templating->set('username', $profile['username']);

			$cake_bit = $user->cake_day($profile['register_date'], $profile['username']);
			$templating->set('cake_icon', $cake_bit);
			
			$their_groups = $user->post_group_list([$profile['user_id']]);
			$profile['user_groups'] = $their_groups[$profile['user_id']];
			$badges = user::user_badges($profile);
			$templating->set('badges', implode(' ', $badges));

            if (isset($_SESSION['user_id']) && $_SESSION['user_id'] > 0)
            {
                $templating->block('top_actions', 'profile');

                $user_action_links = [];

                // give them an edit link if it's their profile
                if ($_SESSION['user_id'] == $profile_id)
                {
                    $user_action_links[] = '<a href="/usercp.php">Click here to edit your profile</a>';						
                }
                
                $user_action_links[] = '<a href="/index.php?module=profile&amp;go=report&amp;user_id='.$profile['user_id'].'">Report Profile</a>';						

                // get blocked id's
                $blocked_ids = [];
                if (count($user->blocked_users) > 0)
                {
                    foreach ($user->blocked_users as $username => $blocked_id)
                    {
                        $blocked_ids[] = $blocked_id[0];
                    }
                }		
                
                if ($_SESSION['user_id'] != $profile_id)
                {
                    $block = '<a href="/index.php?module=block_user&block='.$profile['user_id'].'">Block/Ignore User</a>';
                    if (in_array($profile['user_id'], $blocked_ids))
                    {
                        $block = '<a href="/index.php?module=block_user&unblock='.$profile['user_id'].'">UnBlock User</a>';
					}

					$user_action_links[] = $block;
					
					$message_link = '';
					if ($user->check_group([1,2,5]))
					{
						$message_link = "<a href=\"/direct-messages/compose/user={$profile['user_id']}\">Send Direct Message</a><br />";
					}

                    $user_action_links[] = $message_link;
				}

                $templating->set('user_actions', implode(' | ', $user_action_links));
            }

            // check blocked list
            $blocked = $dbl->run("SELECT `blocked_id` FROM `user_block_list` WHERE `user_id` = ? AND `blocked_id` = ?", array($profile_id, $_SESSION['user_id']))->fetchOne();
            if (($blocked || $profile['private_profile'] == 1) && !$user->check_group([1,2,5]) && (isset($_SESSION['user_id']) && $_SESSION['user_id'] != $profile_id))
            {
                $core->message("Sorry, this user has set their profile to private.", 1);
            }
            else
            {
                if ($profile['banned'] == 1 && $user->check_group([1,2,5]) == false)
                {
                    $core->message("That user is banned so you may not view their profile!", 1);
                }

                else if (($profile['banned'] == 1 && $user->check_group([1,2,5]) == true) || $profile['banned'] == 0)
                {
                    if ($profile['banned'] == 1)
                    {
                        $core->message("You are viewing a banned users profile!", 2);
                    }

                    $templating->set_previous('meta_description', "Viewing {$profile['username']} profile on GamingOnLinux.com", 1);

                    $templating->block('main', 'profile');

                    $registered_date = $core->human_date($profile['register_date']);
                    $templating->set('registered_date', $registered_date);
                    $templating->set('last_login', $core->human_date($profile['last_login']));

                    $avatar = $user->sort_avatar($profile);

                    $templating->set('avatar', $avatar);
                    $templating->set('article_comments', $profile['comment_count']);
                    
                    $their_groups = $dbl->run("SELECT `group_id` FROM `user_group_membership` WHERE `user_id` = ?", array($profile_id))->fetch_all(PDO::FETCH_COLUMN);

                    $allowed_to_find = [1,2,5];
                    $view_find_on = 0;
                    foreach ($their_groups as $check)
                    {
                        if (in_array($check, $allowed_to_find))
                        {
                            $view_find_on = 1;
                        }
                    }

                    if ($view_find_on == 1)
                    {
                        $templating->block('find_on', 'profile');
                        $profile_fields_output = '';
                        foreach ($profile_fields as $name => $field)
                        {
                            if (!empty($profile[$field['db_field']]) && $field['name'] != 'Distro')
                            {
                                $profile[$field['db_field']] = htmlspecialchars($profile[$field['db_field']]);

                                $url = '';
                                $rel = '';

                                if ($field['plain_link'] == 1 && (strpos($profile[$field['db_field']], 'https://') === false && strpos($profile[$field['db_field']], 'http://') === false))
                                {
                                    $url .= 'http://';
                                }
                                
                                if ($field['base_link_required'] == 1 && strpos($profile[$field['db_field']], $field['base_link']) === false ) //base_link_required and not already in the database
                                {
                                    $url = $field['base_link'];
                                }

                                $image = '';
                                if (isset($field['image']) && $field['image'] != NULL)
                                {
                                    $image = "<img src=\"{$field['image']}\" alt=\"{$field['name']}\" />";
                                }

                                $span = '';
                                if (isset($field['span']))
                                {
                                    $span = $field['span'];
                                }

                                if ($name == 'website' || $name == 'mastodon' || $name == 'twitter')
                                {
                                    $rel = ' rel="me" ';
                                }						
                            
                                $profile_fields_output .= "$image$span {$field['name']} <a $rel href=\"$url{$profile[$field['db_field']]}\" target=\"_blank\">$url{$profile[$field['db_field']]}</a><br />";
                            }
                        }

                        $templating->set('profile_fields', $profile_fields_output);

                        $email = '';
                        if ($user->check_group([1,2]) == true)
                        {
                            $email = "Email: {$profile['email']}<br />";
                        }
                        $templating->set('email', $email);
                    }

                    // additional profile info
                    if ($profile['pc_info_public'] == 1)
                    {
						$templating->block('additional', 'profile');
						
						$edit_pcinfo = '';
						if (isset($_SESSION['user_id']) && $_SESSION['user_id'] == $profile['user_id'])
						{
							$edit_pcinfo = '<span style="float: right"><a href="/usercp.php?module=pcinfo">Edit</a></span>';
						}
						$templating->set('edit_pcinfo', $edit_pcinfo);

                        $templating->set('username', $profile['username']);

                        $fields_output = '';
                        $pc_info = $user->display_pc_info($profile['user_id'], $profile['distro']);
                        if ($pc_info['counter'] > 0)
                        {
                            foreach ($pc_info as $k => $info)
                            {
                                if ($k != 'counter' && $k != 'date_updated' && $k != 'include_in_survey' && $k != 'empty')
                                {
                                    $fields_output .= '<li>' . $info . '</li>';
                                }
                            }
                        }
                        else
                        {
                            $fields_output = '<li><em>This user has not filled out their PC info!</em></li>';
                        }

                        $templating->set('fields', $fields_output);
                    }

                    // gather latest articles
                    $article_res = $dbl->run("SELECT `article_id`, `title`, `slug`, `date` FROM `articles` WHERE `author_id` = ? AND `admin_review` = 0 AND `active` = 1 ORDER BY `date` DESC LIMIT 5", array($profile['user_id']))->fetch_all();
                    if ($article_res)
                    {
                        $templating->block('articles_top');
                        foreach ($article_res as $article_link)
                        {
                            $templating->block('articles');

                            $templating->set('latest_article_link', '<a href="' . $article_class->article_link(array('date' => $article_link['date'], 'slug' => $article_link['slug'])).'">'.$article_link['title'].'</a>');
                        }
                        $templating->block('articles_bottom');
                        $templating->set('user_id', $profile['user_id']);
                        $templating->set('username', $profile['username']);
						$templating->set('url', url);
                    }

                    $comment_posts = '';
                    $view_more_comments = '';
                    $comments_execute = $dbl->run("SELECT comment_id, c.`comment_text`, c.`article_id`, c.`time_posted`, a.`title`, a.`slug`, a.`comment_count`, a.`active`, a.`date` FROM `articles_comments` c FORCE INDEX(PRIMARY) INNER JOIN `articles` a ON c.article_id = a.article_id WHERE a.active = 1 AND c.approved = 1 AND c.author_id = ? ORDER BY c.`comment_id` DESC limit 5", array($profile['user_id']))->fetch_all();

                    if ($comments_execute)
                    {
                        $total_comments = count($comments_execute);

                        // comments block
                        $templating->block('article_comments_list', 'profile');

                        foreach ($comments_execute as $comments)
                        {
                            $date = $core->human_date($comments['time_posted']);
                            $title = $comments['title'];

                            // remove quotes, it's not their actual comment, and can leave half-open quotes laying around
                            $text = preg_replace('/\[quote\=(.+?)\](.+?)\[\/quote\]/is', "", $comments['comment_text']);
                            $text = preg_replace('/\[quote\](.+?)\[\/quote\]/is', "", $text);
                            
                            $article_link = $article_class->article_link(array('date' => $comments['date'], 'slug' => $comments['slug'], 'additional' => 'comment_id=' . $comments['comment_id']));

                            $comment_posts .= "<li class=\"list-group-item\">
                        <a href=\"".$article_link."\">{$title}</a>
                        <div>".substr(strip_tags($bbcode->parse_bbcode($text)), 0, 63)."&hellip;</div>
                        <small>{$date}</small>
                    </li>";
                        }

                        if ($total_comments >= 5)
                        {
                            $view_more_comments = '<li class="list-group-item"><a href="/profiles/'.$profile['user_id'].'/comments/">View more comments</a></li>';
                        }
                    }

                    $templating->set('view_more_comments', $view_more_comments);

                    $templating->set('comment_posts', $comment_posts);

                    // list any warnings
                    if ($_SESSION['user_id'] == $profile['user_id'] || $user->can('warn_users'))
                    {
                        $warnings = $dbl->run("SELECT u.`user_id`, u.`username`, w.`warning_text`, w.`warning_ends`, w.`warning_id` FROM `user_warnings` w INNER JOIN `users` u ON w.`warned_by_id` = u.`user_id` WHERE w.`user_id` = ? ORDER BY `warning_ends` DESC", array($profile['user_id']))->fetch_all();
                        if ($warnings)
                        {
                            $warning_list = '';
                            $templating->block('warnings', 'profile');
                            foreach ($warnings as $warn)
                            {
                                $remove_warning = '';
                                if ($user->can('warn_users'))
                                {
                                    $remove_warning = '<span class="fright"><a href="#" class="delete_warning" title="Remove Warning" data-user-id="'.$profile['user_id'].'" data-note-id="'.$warn['warning_id'].'">&#10799;</a></span>';
                                }
                                $warning_list .= '<li id="note-'.$warn['warning_id'].'">'. $remove_warning . 'User <a href="">'.$warn['username'].'</a> sent you a warning for: ' . $warn['warning_text'] . '. Warning is valid until: ' . $warn['warning_ends'] . '</li>';
                            }
                            $templating->set('warning_list', $warning_list);
                        }
                    }

                    // work out any moderator options, and show moderator block if needed
                    $moderation_options = array();

                    if ($user->can('edit_users'))
                    {
                        $moderation_options[] = "<button type=\"submit\" formaction=\"/admin.php?module=users&view=edituser&user_id={$profile['user_id']}\" class=\"btn btn-primary\">Edit User</button>";
                    }
                    if ($user->can('ban_users'))
                    {
                        if ($profile['banned'] == 0)
                        {
                            $moderation_options[] = "<button name=\"act\" value=\"ban\" class=\"btn btn-danger\">Ban User</button>";
                        }
                        if ($profile['banned'] == 1)
                        {
                            $moderation_options[] = "<button name=\"act\" value=\"delete_user_content\"  class=\"btn btn-danger\">Delete user content</button>";
                        }
                    }

                    if ($user->can('warn_users'))
                    {
                        $moderation_options[] = "<button type=\"submit\" formaction=\"/admin.php?module=users&view=warn&user_id={$profile['user_id']}\" class=\"btn btn-primary\">Warn User</button>";
                    }

                    if (!empty($moderation_options))
                    {
                        $templating->block('end', 'profile');
                        $templating->set('admin_links', '<form method="post" action="/admin.php?module=users&user_id=' . $profile['user_id'] . '">' . implode(' ' , $moderation_options) . '<input type="hidden" name="ip" value="'.$profile['ip'].'" /></form>');
                    }
                }
            }
        }
    }
    
    if (isset(core::$url_command[2]) && core::$url_command[2] == 'comments')
    {
        if ($user->check_group([1,2,5]))
        {
            if (isset($profile_id) && is_numeric($profile_id))
            {
                $templating->set_previous('canonical_link', '<link rel="canonical" href="'.$core->config('website_url') . 'profiles/'.$profile_id.'/comments/">', 1);

                $get_username = $dbl->run("SELECT `username`, `private_profile` FROM `users` WHERE `user_id` = ?", array($profile_id))->fetch();
                if ($get_username)
                {
                    $templating->set_previous('title', 'Viewing comments for - ' . $get_username['username'], 1);

                    // check blocked list
                    $blocked = $dbl->run("SELECT `blocked_id` FROM `user_block_list` WHERE `user_id` = ? AND `blocked_id` = ?", array($profile_id, $_SESSION['user_id']))->fetchOne();
                    if (($blocked || $get_username['private_profile'] == 1) && !$user->check_group([1,2,5]) && (isset($_SESSION['user_id']) && $_SESSION['user_id'] != $profile_id))
                    {
                        $core->message("Sorry, this user has set their profile to private.", 1);
                    }
                    else
                    {
                        // count how many there is in total
                        $total = $dbl->run("SELECT COUNT(`comment_id`) FROM `articles_comments` WHERE `author_id` = ?", array($profile_id))->fetchOne();
                            
                        $page = core::give_page();

                        // sort out the pagination link
                        $pagination = $core->pagination_link(10, $total, $core->config('website_url')  . "profiles/".$profile_id."/comments/", $page);

                        // get top of comments section
                        $templating->block('more_comments');
                        $templating->set('username', $get_username['username']);
                        $templating->set('profile_link', "/profiles/" . $profile_id);

                        $comment_posts = '';
                        $all_comments = $dbl->run("SELECT comment_id, c.`comment_text`, c.`article_id`, c.`time_posted`, a.`title`, a.`slug`, a.`comment_count`, a.`active`, a.`date` FROM `articles_comments` c INNER JOIN `articles` a ON c.`article_id` = a.`article_id` WHERE a.`active` = 1 AND c.`author_id` = ? ORDER BY c.`comment_id` DESC LIMIT ?, 10", array($profile_id, $core->start))->fetch_all();
                            
                        // make an array of all comment ids to search for likes (instead of one query per comment for likes)
                        $like_array = [];
                        $sql_replacers = [];
                        foreach ($all_comments as $id_loop)
                        {
                            $like_array[] = $id_loop['comment_id'];
                            $sql_replacers[] = '?';
                        }
                        if (!empty($sql_replacers))
                        {
                            // Total number of likes for the comments
                            $get_likes = $dbl->run("SELECT data_id, COUNT(*) FROM likes WHERE data_id IN ( ".implode(',', $sql_replacers)." ) AND `type` = 'comment' GROUP BY data_id", $like_array)->fetch_all(PDO::FETCH_COLUMN|PDO::FETCH_GROUP);
                        }
                            
                        foreach ($all_comments as $comments)
                        {
                            $date = $core->human_date($comments['time_posted']);
                            $title = $comments['title'];
                                
                            // sort out the likes
                            $likes = NULL;
                            if (isset($get_likes[$comments['comment_id']]))
                            {
                                $likes = ' <span class="profile-comments-heart icon like"></span> Likes: ' . $get_likes[$comments['comment_id']][0];
                            }
                            
                            $view_comment_link = $article_class->article_link(array('date' => $comments['date'], 'slug' => $comments['slug'], 'additional' => 'comment_id=' . $comments['comment_id']));
                            $view_article_link = $article_class->article_link(array('date' => $comments['date'], 'slug' => $comments['slug']));
                            $view_comments_full_link = $article_class->article_link(array('date' => $comments['date'], 'slug' => $comments['slug'], 'additional' => '#comments'));

                            $comment_posts .= "<div class=\"box\"><div class=\"body group\">
                            <a href=\"".$view_comment_link."\">{$title}</a><br />
                            <small>{$date}" . $likes ."</small><br />
                            <hr />
                            <div>".$bbcode->parse_bbcode($comments['comment_text'])."</div>
                            <hr />
                            <div><a href=\"".$view_comment_link."\">View this comment</a> - <a href=\"".$view_article_link."\">View article</a> - <a href=\"".$view_comments_full_link."\">View full comments</a></div>
                            </div></div>";
                        }

                        $templating->set('comment_posts', $comment_posts);
                        $templating->set('pagination', $pagination);
                    }
                }
                else
                {
                    $core->message('User does not exist!');
                }
            }
            else
            {
                $core->message('User ID was not a number!');
            }
		}
        else
        {
            $_SESSION['message'] = 'no_permission';
            header("Location: /");
            die();
        }
    }
}

if (isset($_POST['act']) && $_POST['act'] == 'report_profile')
{
    if (isset($_GET['user_id']) && is_numeric($_GET['user_id']))
    {
        $find_user = $dbl->run("SELECT `user_id` FROM `users` WHERE `user_id` = ?", array($_GET['user_id']))->fetch();
        if ($find_user)
        {
            if (!isset($_POST['reason']))
			{
                $_SESSION['form_error'] = $_POST;
				$_SESSION['message'] = 'empty';
				$_SESSION['message_extra'] = 'reason';
				header("Location: /index.php?module=profile&go=report&user_id=".$_GET['user_id']);
				die();
			}

			$reasons = array('Inappropriate', 'Spam', 'Illegal', 'Misc');
			if (!in_array($_POST['reason'], $reasons))
			{
                $_SESSION['form_error'] = $_POST;
				$_SESSION['message'] = 'empty';
				$_SESSION['message_extra'] = 'reason';
				header("Location: /index.php?module=profile&go=report&user_id=".$_GET['user_id']);
				die();				
			}

			if ($_POST['reason'] == 'Illegal')
			{
				$illegal_reason = trim($_POST['illegal_reason']);
				if (empty($illegal_reason))
				{
                    $_SESSION['form_error'] = $_POST;
					$_SESSION['message'] = 'empty';
					$_SESSION['message_extra'] = 'illegal reason textarea';
					header("Location: /index.php?module=profile&go=report&user_id=".$_GET['user_id']);
					die();						
				}

				$illegal_reason = '<br />The noted explanation from the user on the illegal content is as follows:<br /><details class="spoiler_container"><summary class="spoiler_header">Click to see text</summary><div class="spoiler-content">'. $illegal_reason . '</div></details>';
			}

            $core->new_admin_note(array('completed' => 0, 'content' => $illegal_reason, 'type' => 'reported_profile', 'type_additional' => $_POST['reason'], 'data' => $_GET['user_id']));

            $dbl->run("UPDATE `users` SET `reported` = 1 WHERE `user_id` = ?", array($_GET['user_id']));

            unset($_SESSION['form_error']);

            $_SESSION['message'] = 'reported';
			$_SESSION['message_extra'] = 'profile';

			header("Location: ".$core->config('website_url'));
			die();
        }        
    }
}