<?php
if(!defined('golapp')) 
{
	die('Direct access not permitted');
}

// meta tags for g+, facebook and twitter images
$templating->set_previous('meta_data', "<meta property=\"og:title\" content=\"GamingOnLinux Anti-Cheat Compatibility List for Steam Deck and Linux\" />
<meta property=\"og:description\" content=\"A page dedicated to showing you the expected compatibility for Steam Deck and Linux desktop with games that have anti-cheat enabled.\" />
<meta property=\"og:image\" content=\"https://www.gamingonlinux.com/templates/default/images/logos/twitter_card_icon.png\"/>
<meta property=\"og:image_url\" content=\"https://www.gamingonlinux.com/templates/default/images/logos/twitter_card_icon.png\"/>", 1);

$templating->load('anticheat');
$templating->block('anticheat_list_top');
$templating->set('discord_link', $core->config('discord'));

$total = $dbl->run("SELECT count(`id`) FROM `calendar` WHERE `anticheat_status` IS NOT NULL")->fetchOne();

$templating->set("total", $total);

$templating->block('social_media');

$updates_limit = 5;
if (isset(core::$url_command[1]) && core::$url_command[1] == 'updates')
{
	$templating->set_previous('meta_description', 'GamingOnLinux Anti-Cheat Compatibility List for Steam Deck and Linux, latest updates.', 1);
	$templating->set_previous('title', 'Anti-Cheat Compatibility List for Steam Deck and Linux - latest updates', 1);
	$templating->set_previous('canonical_link', '<link rel="canonical" href="'.$core->config('website_url') . 'anticheat/updates/">', 1);
	$templating->block('breadcrumb_updates');
	$templating->set('url', url);

	$updates_limit = 50;
}

if (!isset(core::$url_command[1]) || isset(core::$url_command[1]) && core::$url_command[1] != 'vendor')
{
	$templating->block('updates_top');

	$get_updates = $dbl->run("SELECT `name`, `id`, `anticheat_status`, `anticheat_status_date`, `single_player_works`, `anticheat_proton`, `anticheat_native` FROM `calendar` WHERE `anticheat_status` IS NOT NULL AND `anticheat_status_date` IS NOT NULL ORDER BY `anticheat_status_date` DESC LIMIT $updates_limit")->fetch_all();

	if (!empty($get_updates))
	{
		$item_ids = array();
		foreach ($get_updates as $item)
		{
			$item_ids[] = $item['id'];
		}

		$items_in  = str_repeat('?,', count($item_ids) - 1) . '?';

		// get list of anti-cheats for each item
		$anticheats_list = $dbl->run("SELECT v.`name`, a.`anticheat_id`, a.`item_db` FROM `itemdb_anticheats` a INNER JOIN `anticheat_vendors` v ON a.anticheat_id = v.id WHERE a.`item_db` IN ($items_in)", $item_ids)->fetch_all();
		$anticheat_names = array();
		if (isset($anticheats_list) && !empty($anticheats_list))
		{
			foreach ($anticheats_list as $vendor)
			{
				$anticheat_names[$vendor['item_db']][] = '<a href="/anticheat/vendor/'.$vendor['anticheat_id'].'">'.$vendor['name'] . '</a>';
			}
		}

		// get total comments
		$get_comments = $dbl->run("SELECT `replys`, `itemdb_anticheat` FROM `forum_topics` WHERE `itemdb_anticheat` IN ($items_in)", $item_ids)->fetch_all();
		$list_comments = array();
		if (isset($get_comments) && !empty($get_comments))
		{
			foreach ($get_comments as $comment)
			{
				$list_comments[$comment['itemdb_anticheat']] = $comment['replys'];
			}
		}

		foreach ($get_updates as $item)
		{
			if (!isset($list_comments[$item['id']]))
			{
				$list_comments[$item['id']] = 0;
			}

			$templating->block('updates_item');
			$templating->set('name', $item['name']);
			$templating->set('link', '/itemdb/app/'.$item['id']);
			$edit = '';
			if ($user->check_group([1,2,5]))
			{
				$edit = '<a href="/admin.php?module=games&view=edit&id='.$item['id'].'&return=anticheat"><span class="icon edit edit-sale-icon"></span></a> ';
			}
			$templating->set('edit', $edit);

			if (isset($anticheat_names[$item['id']]))
			{
				$templating->set('anticheat_vendor', implode(', ', $anticheat_names[$item['id']]));
			}
			else
			{
				$templating->set('anticheat_vendor', '');
			}

			if ($item['anticheat_status'] == 0)
			{
				$status = '<div class="anticheat-icon broken"><img src="/templates/default/images/cross.svg" width="30" height="30" /></div>';
			}
			if ($item['anticheat_status'] == 1)
			{
				$status = '<div class="anticheat-icon works"><img src="/templates/default/images/thumbsup.svg" width="25" height="25" /></div>';
			}
			if ($item['single_player_works'] == 1)
			{
				$status .= '<div class="anticheat-icon info" title="Single Player Works"><img src="/templates/default/images/1player.svg" width="30" height="30" /></div>';
			}
			$templating->set('linux_status', $status);

			$anticheat_proton = '';
			if ($item['anticheat_proton'] == 1)
			{
				$anticheat_proton = '<div class="anticheat-icon-platform"><img src="/templates/default/images/proton_icon.png" alt="Proton" title="Works with Proton" /></div>';
			}
			$anticheat_native = '';
			if ($item['anticheat_native'] == 1)
			{
				$anticheat_native = '<div class="anticheat-icon-platform"><img src="/templates/default/images/tux_icon2.svg" height="20" alt="Proton" title="Works with Native Linux" /></div>';
			}		
			$templating->set('proton', $anticheat_proton);
			$templating->set('native', $anticheat_native);

			$comments_total = number_format($list_comments[$item['id']]);
			
			$templating->set('comments_total', $comments_total);

			$templating->set('comments_link', url.'forum/anticheat/'.$item['id']);

			$templating->set('status_value', $item['anticheat_status']);

			$templating->set('updated_date', $item['anticheat_status_date']);
		}		
	}

	if (!isset(core::$url_command[1]))
	{
		$templating->block('more_updates');
	}

	$templating->block('updates_bottom');
}

/*
// top articles this month but not from the most recent 2 days to prevent showing what they've just seen on the home page
*/
$blocked_tags  = str_repeat('?,', count($user->blocked_tags) - 1) . '?';

$top_article_query = "SELECT a.`article_id`, a.`title`, a.`slug`, a.`date`, a.`tagline_image`, a.`gallery_tagline`, t.`filename` as gallery_tagline_filename FROM `article_category_reference` r JOIN `articles` a ON a.`article_id` = r.`article_id` LEFT JOIN `articles_tagline_gallery` t ON t.`id` = a.`gallery_tagline` WHERE r.`category_id` = 200 AND NOT EXISTS (SELECT 1 FROM article_category_reference c  WHERE a.article_id = c.article_id AND c.`category_id` IN ( $blocked_tags )) ORDER BY a.`date` DESC LIMIT 4";

$fetch_recent = $dbl->run($top_article_query, $user->blocked_tags)->fetch_all();

if (is_array($fetch_recent) && count($fetch_recent) === 4)
{
	$templating->block('recent-articles', 'anticheat');
	$article_list = '';
	foreach ($fetch_recent as $recent)
	{
		$tagline_image = $article_class->tagline_image($recent, 142, 250);

		$article_list .= '<div class="top-missed-item"><div class="article-top-missed-image"><a href="'.$article_class->article_link(array('date' => $recent['date'], 'slug' => $recent['slug'])).'">'.$tagline_image.'</a></div><div><strong><a href="'.$article_class->article_link(array('date' => $recent['date'], 'slug' => $recent['slug'])).'">'.$recent['title'].'</a></strong></div></div>';
	}

	$templating->set('article_list', $article_list);
	$templating->set('url', url);
}

if (!isset(core::$url_command[1])) // for showing all
{
	$templating->set_previous('meta_description', 'GamingOnLinux Anti-Cheat Compatibility List for Steam Deck and Linux', 1);
	$templating->set_previous('title', 'Anti-Cheat Compatibility List for Steam Deck and Linux', 1);
	$templating->set_previous('canonical_link', '<link rel="canonical" href="'.$core->config('website_url') . 'anticheat/">', 1);

	$get_items = $dbl->run("SELECT `name`, `id`, `anticheat_status`, `single_player_works`, `anticheat_proton`, `anticheat_native` FROM `calendar` WHERE `anticheat_status` IS NOT NULL ORDER BY `name` ASC")->fetch_all();

	$templating->block('title_top');
	$templating->set('anticheat_name', 'any Anti-Cheat');
}
else if (isset(core::$url_command[1]) && core::$url_command[1] == 'vendor' && isset(core::$url_command[2]) && is_numeric(core::$url_command[2])) // for showing specific anti-cheat vendor only
{
	// make sure they exist
	$check_anticheat = $dbl->run("SELECT `id`, `name` FROM `anticheat_vendors` WHERE `id` = ?", array(core::$url_command[2]))->fetch();
	if (!$check_anticheat)
	{
		$_SESSION['message'] = 'none_found';
		$_SESSION['message_extra'] = 'anticheat vendors matching that ID';
		header("Location: /index.php");
		die();
	}

	$templating->set_previous('meta_description', 'Anti-Cheat Compatibility List for Steam Deck and Linux: '.$check_anticheat['name'], 1);
	$templating->set_previous('title', $check_anticheat['name'] . ' compatibility list for Steam Deck and Linux', 1);
	$templating->set_previous('canonical_link', '<link rel="canonical" href="'.$core->config('website_url') . 'anticheat/vendor/'.$check_anticheat['id'].'/">', 1);

	$templating->block('breadcrumb');
	$templating->set('url', url);
	$templating->set('anticheat_name', $check_anticheat['name']);

	$templating->block('title_top');
	$templating->set('anticheat_name', $check_anticheat['name']);

	$get_items = $dbl->run("SELECT g.`name`, g.`id`, g.`anticheat_status`, g.`single_player_works`, g.`anticheat_proton`, g.`anticheat_native`, v.name AS `vendor_name` FROM `itemdb_anticheats` a INNER JOIN `calendar` g ON a.`item_db` = g.`id` INNER JOIN `anticheat_vendors` v ON a.`anticheat_id` = v.`id` WHERE a.`anticheat_id` = ?", array(core::$url_command[2]))->fetch_all();
}

if (!empty($get_items))
{
	$item_ids = array();
	foreach ($get_items as $item)
	{
		$item_ids[] = $item['id'];
	}

	$items_in  = str_repeat('?,', count($item_ids) - 1) . '?';

	// get list of developers for each item
	$developers_list = $dbl->run("SELECT r.`developer_id`, d.`name`, r.`game_id` FROM `game_developer_reference` r LEFT JOIN `developers` d ON r.`developer_id` = d.`id` WHERE r.`game_id` IN ($items_in)", $item_ids)->fetch_all();
	$dev_names = array();
	if (isset($developers_list) && !empty($developers_list))
	{
		foreach ($developers_list as $developer)
		{
			$dev_names[$developer['game_id']][] = '<a href="/itemdb/developer/'.$developer['developer_id'].'">'.$developer['name'] . '</a>';
		}
	}

	// get list of anti-cheats for each item
	$anticheats_list = $dbl->run("SELECT v.`name`, a.`anticheat_id`, a.`item_db` FROM `itemdb_anticheats` a INNER JOIN `anticheat_vendors` v ON a.anticheat_id = v.id WHERE a.`item_db` IN ($items_in)", $item_ids)->fetch_all();
	$anticheat_names = array();
	if (isset($anticheats_list) && !empty($anticheats_list))
	{
		foreach ($anticheats_list as $vendor)
		{
			$anticheat_names[$vendor['item_db']][] = '<a href="/anticheat/vendor/'.$vendor['anticheat_id'].'">'.$vendor['name'] . '</a>';
		}
	}

	// get total comments
	$get_comments = $dbl->run("SELECT `replys`, `itemdb_anticheat` FROM `forum_topics` WHERE `itemdb_anticheat` IN ($items_in)", $item_ids)->fetch_all();
	$list_comments = array();
	if (isset($get_comments) && !empty($get_comments))
	{
		foreach ($get_comments as $comment)
		{
			$list_comments[$comment['itemdb_anticheat']] = $comment['replys'];
		}
	}

	foreach ($get_items as $item)
	{
		if (!isset($list_comments[$item['id']]))
		{
			$list_comments[$item['id']] = 0;
		}

		$templating->block('anticheat_list_item');
		$templating->set('name', $item['name']);
		$templating->set('link', '/itemdb/app/'.$item['id'].'/'.$core->nice_title($item['name']).'/');
		$edit = '';
		if ($user->check_group([1,2,5]))
		{
			$edit = '<a href="/admin.php?module=games&view=edit&id='.$item['id'].'&return=anticheat"><span class="icon edit edit-sale-icon"></span></a> ';
		}
		$templating->set('edit', $edit);

		if (isset($dev_names[$item['id']]))
		{
			$templating->set('dev_name', implode(', ', $dev_names[$item['id']]));
		}
		else
		{
			$templating->set('dev_name', '');
		}

		if (isset($anticheat_names[$item['id']]))
		{
			$templating->set('anticheat_vendor', implode(', ', $anticheat_names[$item['id']]));
		}
		else
		{
			$templating->set('anticheat_vendor', '');
		}

		if ($item['anticheat_status'] == 0)
		{
			$status = '<div class="anticheat-icon broken" title="Broken"><img src="/templates/default/images/cross.svg" width="30" height="30" /></div>';
		}
		if ($item['anticheat_status'] == 1)
		{
			$status = '<div class="anticheat-icon works" title="Works"><img src="/templates/default/images/thumbsup.svg" width="25" height="25" /></div>';
		}
		if ($item['single_player_works'] == 1)
		{
			$status .= '<div class="anticheat-icon info" title="Single Player Works"><img src="/templates/default/images/1player.svg" width="30" height="30" /></div>';
		}
		$templating->set('linux_status', $status);

		$anticheat_proton = '';
		if ($item['anticheat_proton'] == 1)
		{
			$anticheat_proton = '<div class="anticheat-icon-platform"><img src="/templates/default/images/proton_icon.png" alt="Proton" title="Works with Proton" /></div>';
		}
		$anticheat_native = '';
		if ($item['anticheat_native'] == 1)
		{
			$anticheat_native = '<div class="anticheat-icon-platform"><img src="/templates/default/images/tux_icon2.svg" height="20" alt="Proton" title="Works with Native Linux" /></div>';
		}		
		$templating->set('proton', $anticheat_proton);
		$templating->set('native', $anticheat_native);

		$comments_total = number_format($list_comments[$item['id']]);

		$templating->set('comments_total', $comments_total);

		$templating->set('comments_link', url.'forum/anticheat/'.$item['id']);

		$templating->set('status_value', $item['anticheat_status']);
	}
	$templating->block('anticheat_list_bottom');
}


$templating->block('latest_comments_top');

$fetch_topics = $dbl->run("SELECT t.`topic_id`, t.`topic_title`, t.`last_post_date`, t.`last_post_id`, t.`itemdb_anticheat`, u.`username` FROM `forum_topics` t INNER JOIN `users` u ON u.user_id = t.last_post_user_id WHERE t.`approved` = 1 AND t.`itemdb_anticheat` IS NOT NULL ORDER BY t.`last_post_date` DESC limit 10")->fetch_all();
if ($fetch_topics)
{
	foreach ($fetch_topics as $topics)
	{
		$date = $core->time_ago($topics['last_post_date']);
		$machine_time = date("Y-m-d\TH:i:s", $topics['last_post_date']);

		$templating->block('comment_row');
		$templating->set('link', url . '/forum/anticheat/'.$topics['itemdb_anticheat'] . '/post_id='.$topics['last_post_id']);
		$templating->set('title', $topics['topic_title']);
		$templating->set('machine_time', $machine_time);
		$templating->set('date', $date);
		$templating->set('username', $topics['username']);
	}
}
else
{
	$core->message("None yet!");
}
$templating->block('latest_comments_bottom', 'anticheat');

$templating->block('legend', 'anticheat');