<?php
if(!defined('golapp')) 
{
	die('Direct access not permitted');
}
$templating->set_previous('title', 'Contact Us', 1);
$templating->set_previous('meta_description', 'Contact Us form for GamingOnLinux', 1);
$templating->set_previous('canonical_link', '<link rel="canonical" href="'.$core->config('website_url') . 'contact-us/">', 1);

$templating->load('contact');
$templating->block('top');

if (isset($_SESSION['user_id']) && $_SESSION['user_id'] > 0)
{
	$submit_link = 'Got the writing bug? You can also submit a full article using the <a href="'.url.'submit-article/">Submit Article area</a>!';
	if ($user->check_group([1,2,5]))
	{
		$submit_link = 'Got the writing bug? You can also submit a full article using the <a href="'.url.'admin.php?module=add_article">Submit Article area</a>!';
	}
}
else
{
	$submit_link = 'Registered users can also write and submit their own articles to be published!';
}

$templating->set('submit_link', $submit_link);
$templating->set('discord_link', $core->config('discord'));
$templating->set('matrix_link', $core->config('matrixchat_address'));