<?php
if(!defined('golapp')) 
{
	die('Direct access not permitted');
}
$templating->set_previous('title', 'GamingOnLinux Referenced Elsewhere', 1);
$templating->set_previous('meta_description', 'A showcase of other sites sourcing and referencing GamingOnLinux', 1);
$templating->set_previous('canonical_link', '<link rel="canonical" href="'.$core->config('website_url') . 'about-us/">', 1);

$templating->load('references');
$templating->block('top');
?>
