<?php
/* TODO
Mention on the user deletion page they can download their data - mention this will not work if they delete their account before doing so
Forum replies
User profile info - should link to index files for other sections?
Articles
Add a field for "date completed", make sure to skip those on the MySQL
Make sure housekeeping cron removes them after 30 days
*/
ini_set("memory_limit", "-1");

define("APP_ROOT", dirname( dirname(__FILE__) ) . '/public_html');

require APP_ROOT . "/includes/bootstrap.php";

// make sure user exists first
$get_users = $dbl->run("SELECT dr.`id`, dr.`user_id`, u.`username`, u.`avatar_uploaded`, u.`avatar`, u.`email` FROM `user_data_request` dr INNER JOIN `users` u ON u.user_id = dr.user_id ORDER BY dr.`date_requested` ASC")->fetch_all();

foreach ($get_users as $user)
{
	$data_folder = APP_ROOT . '/uploads/user_data_request/' . $user['user_id'] . '/';

	if (!file_exists($data_folder)) 
	{
		mkdir($data_folder, 0777, true);
		@chmod($data_folder, 0777);
	}

	$zip = new ZipArchive();

	$zip_name = $user['user_id'].time().core::random_id().'.zip';

	/* article comments */
	$comment_index_text = 'Here is a list of files showing your comments on <a href="https://www.gamingonlinux.com">GamingOnLinux.com</a>. Any problems with this archive please email contact@gamingonlinux.com.';

	$comments = $dbl->run("SELECT c.comment_text, c.time_posted, a.title FROM `articles_comments` c LEFT JOIN `articles` a ON c.article_id = a.article_id WHERE c.`author_id` = ?", array($user['user_id']))->fetch_all();

	$total_comments = count($comments);

	$comments_files = [];

	$counter = 0;
	$file_number = 0;
	if ($total_comments > 0)
	{
		$comment_index_text .= '<ul>';
		
		while($comment_loop = array_splice($comments, 0, 50)) 
		{
			$comment_text = "<root>\r\n";

			$file_number++;
			foreach($comment_loop as $comment)
			{
				$counter++;
				$comment_date = date('Y-m-d H:i:s', $comment['time_posted']);

				$title = '';
				if (isset($comment['title']))
				{
					$title = $comment['title'];
				}
				else
				{
					$title = 'Couldn\'t get article title, possibly a deleted article';
				}

				$comment_text .= "\r\n<item>\r\n<heading><![CDATA[Article title: ".$title."]]></heading>\r\n<date>".$comment_date."</date>\r\n<body><![CDATA[".$comment['comment_text']."]]></body>\r\n</item>\r\n";
			}

			$comments_file = $data_folder . $user['user_id'] . 'comments_'.$file_number.'.xml';
			$comment_text .= "\r\n</root>";
			file_put_contents($comments_file, $comment_text);
			$comments_files[$comments_file] = $user['user_id'] . 'comments_'.$file_number.'.xml';
			
			unset($comment_text);

			$comment_index_text .= '<li><a href="' . $user['user_id'] . 'comments_'.$file_number.'.xml">File: '.$file_number.'</a></li>';
		}
		$comment_index_text .= '</ul>';

		// make an index file with links to the other files, zip it, remove the basic file
		$comment_index_filename = $data_folder . $user['user_id'] . 'comments_index.html';

		file_put_contents($comment_index_filename, $comment_index_text);

		if ($zip->open($data_folder.$zip_name, ZipArchive::CREATE) === TRUE) 
		{
			$zip->addFile($comment_index_filename, 'article_comments/index.html');
			$zip->close();
			echo 'ok';
		} 
		else 
		{
			error_log ( 'Failed making zip file for user data request' );
		}

		unlink($comment_index_filename);

		// now add each comments file to the zip, then remove the basic file
		foreach ($comments_files as $full_file_path => $file_name)
		{
			if ($zip->open($data_folder.$zip_name, ZipArchive::CREATE) === TRUE) 
			{
				$zip->addFile($full_file_path, 'article_comments/'.$file_name);
				$zip->close();
				echo 'ok';
			} 
			else 
			{
				error_log ( 'Failed making zip file for user data request' );
			}
			unlink($full_file_path);
		}
	}

	/* user avatar if uploaded */

	if ($user['avatar_uploaded'] == 1)
	{
		$avatar_path = APP_ROOT . '/uploads/avatars/' . $user['avatar'];
		if (file_exists($avatar_path))
		{
			if ($zip->open($data_folder.$zip_name, ZipArchive::CREATE) === TRUE) 
			{
				$zip->addFile($avatar_path, '/avatar/'.$user['avatar']);
				$zip->close();
				echo 'ok';
			} 
			else 
			{
				error_log ( 'Failed making adding user avatar to zip file for user data request' );
			}
		}
	}

	/* forum topics */

	$forum_topics = $dbl->run("SELECT t.`topic_title`, t.`creation_date`, p.`reply_text` FROM `forum_topics` t INNER JOIN `forum_replies` p ON p.topic_id = t.topic_id AND p.is_topic = 1 WHERE t.`author_id` = ?", array($user['user_id']))->fetch_all();

	$total_topics = count($forum_topics);

	$topics_index_text = 'Here is a list of files showing your forum topics on <a href="https://www.gamingonlinux.com">GamingOnLinux.com</a>. Any problems with this archive please email contact@gamingonlinux.com.';

	$topics_files = [];

	$counter = 0;
	$file_number = 0;
	if ($total_topics > 0)
	{
		$topics_index_text .= '<ul>';
		while($topics_loop = array_splice($forum_topics, 0, 50)) 
		{
			$topics_text = '<root>';
			$file_number++;
			foreach($topics_loop as $topic)
			{
				$counter++;
				$topic_date = date('Y-m-d H:i:s', $topic['creation_date']);

				$title = $topic['topic_title'];

				$topics_text .= "\r\n<item>\r\n<header><![CDATA[".$title."]]></header>\r\n<date>".$topic_date."</date>\r\n<body><![CDATA[".$topic['reply_text']."]]></body>\r\n</item>\r\n";
			}

			$topics_text .= "\r\n</root>";

			$topics_file = $data_folder . $user['user_id'] . 'topics_'.$file_number.'.xml';
			file_put_contents($topics_file, $topics_text);
			$topics_files[$topics_file] = $user['user_id'] . 'topics_'.$file_number.'.xml';
			unset($topics_text);

			$topics_index_text .= '<li><a href="' . $user['user_id'] . 'topics_'.$file_number.'.xml">File: '.$file_number.'</a></li>';
		}
		$topics_index_text .= '</ul>';

		// make an index file with links to the other files, zip it, remove the basic file
		$topics_index_filename = $data_folder . $user['user_id'] . 'forum_topics_index.html';

		file_put_contents($topics_index_filename, $topics_index_text);

		if ($zip->open($data_folder.$zip_name, ZipArchive::CREATE) === TRUE) 
		{
			$zip->addFile($topics_index_filename, 'forum_topics/index.html');
			$zip->close();
			echo 'ok';
		} 
		else 
		{
			error_log ( 'Failed making zip file for user data request' );
		}

		unlink($topics_index_filename);

		// now add each comments file to the zip, then remove the basic file
		foreach ($topics_files as $full_file_path => $file_name)
		{
			if ($zip->open($data_folder.$zip_name, ZipArchive::CREATE) === TRUE) 
			{
				$zip->addFile($full_file_path, 'forum_topics/'.$file_name);
				$zip->close();
				echo 'ok';
			} 
			else 
			{
				error_log ( 'Failed making forum topics zip file for user data request' );
			}
			unlink($full_file_path);
		}
	}

	/* forum replies */

	/* user profile */

	/* PC Info */
	$pc_info = $dbl->run("SELECT `date_updated`, `last_update_reminder`, `desktop_environment`, `dual_boot`, `ram_count`, `cpu_vendor`, `cpu_model`, `gpu_vendor`, `gpu_model`, `gpu_driver`, `gpu_driver_version`, `monitor_count`, `resolution`, `gaming_machine_type`, `gamepad`, `vrheadset`, `session_type`, `include_in_survey`, `refresh_rate`, `vrr` FROM `user_profile_info` WHERE `user_id` = ?", array($user['user_id']))->fetch();

	$pc_info_output = "<root><item>\r\n";

	foreach ($pc_info as $key => $item)
	{
		$pc_info_output .= "\r\n<$key><![CDATA[$item]]></$key>\r\n";
	}

	$pc_info_output .= "</item>\r\n</root>";

	$pc_info_file = $data_folder . $user['user_id'] . 'pc_info.xml';

	file_put_contents($pc_info_file, $pc_info_output);

	if ($zip->open($data_folder.$zip_name, ZipArchive::CREATE) === TRUE) 
	{
		$zip->addFile($pc_info_file, '/pcinfo/pc_info.xml');
		$zip->close();
		echo 'ok';
		unlink($pc_info_file);
	} 
	else 
	{
		error_log ( 'Failed making adding user avatar to zip file for user data request' );
	}

	echo 'Download: <a href="https://www.gamingonlinux.com/uploads/user_data_request/'.$user['user_id'].'/'.$zip_name.'">https://www.gamingonlinux.com/uploads/user_data_request/'.$user['user_id'].'/'.$zip_name.'</a>';

	// check if they have admin notes?

	// check for any admin notifications?

	// update database
	$dbl->run("UPDATE `user_data_request` SET `filename` = ? WHERE `id` = ?", array($zip_name, $user['id']));

	// email user the link

	$subject = 'Your GamingOnLinux user data request';

	// message
	$html_message = '<p>Hello, your data access request for GamingOnLinux is complete. You may find it here: <a href="https://www.gamingonlinux.com/uploads/user_data_request/'.$user['user_id'].'/'.$zip_name.'">https://www.gamingonlinux.com/uploads/user_data_request/'.$user['user_id'].'/'.$zip_name.'</a></p><p>Please note that these files are removed after 30 days.</p>';

	$plain_message = 'Hello, your data access request for GamingOnLinux is complete. You may find it here: https://www.gamingonlinux.com/uploads/user_data_request/'.$user['user_id'].'/'.$zip_name . '. Please note that these files are removed after 30 days.';

	// Mail it
	if ($core->config('send_emails') == 1)
	{
		$mail = new mailer($core);
		$mail->sendMail($user['email'], $subject, $html_message, $plain_message);
	}
}